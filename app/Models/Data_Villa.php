<?php
  
namespace App\Models;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Database\Eloquent\Casts\Attribute;
use App\Models\Video;
class Data_Villa extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;
  protected $table = 'data_villa';
    /**
     * The attributes that are mass assignable.
     *
     * @var array

     */
    protected $fillable = [
        'Nama_Villa',
        'Deskripsi',
        'Jumlah_Kamar',
        'Max',
        'Harga_Weekend',
        'Harga_Weekday',
        'Alamat',
        'Fasilitas'
       
    ];

    public function gambarVilla()
    {
        return $this->hasMany(Gambar_Villa::class, 'id_villa', 'id');
    }
    
    public function vid()
    {
        return $this->hasMany(Video::class, 'villa_id','id');
    }
}
