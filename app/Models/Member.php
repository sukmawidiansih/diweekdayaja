<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Member extends Model
{
    protected $table = 'members'; // Menyatakan nama tabel yang sesuai

    protected $fillable = [
        'user_id', // Isi field yang bisa diisi secara massal
        'phone',
        'gender',
        'photo'
        // Tambahkan field lainnya jika diperlukan
    ];

    // Relasi dengan model User jika ada
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}