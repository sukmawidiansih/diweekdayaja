<?php
  
namespace App\Models;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Database\Eloquent\Casts\Attribute;
use App\Models\blog;
  
class kategori extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;
  protected $table = 'kategori';
    /**
     * The attributes that are mass assignable.
     *
     * @var array

     */
    protected $fillable = [
        'nama_kategori'
    ];

    public function blogs()
    {
        return $this->hasMany(Blog::class, 'id_kategori');
    }

   
}