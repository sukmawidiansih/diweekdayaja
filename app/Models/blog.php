<?php
  
namespace App\Models;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Database\Eloquent\Casts\Attribute;
use App\Models\kategori;
  
class blog extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;
  protected $table = 'blog';
    /**
     * The attributes that are mass assignable.
     *
     * @var array

     */
    // Blog.php (Model)
    protected $fillable = ['judul', 'gambar', 'deskripsi' , 'id_kategori'];
    // public function kategori()
    // {
    //     return $this->belongsTo(Kategori::class, 'id_kategori');
    // }

    public function kategori()
    {
        return $this->belongsTo(kategori::class, 'id_kategori');
    }
}