<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Data_Villa;
class video extends Model
{
    protected $table = 'videos';

    protected $fillable = ['media', 'villa_id'];

    // Relasi dengan model Data_Villa
    public function villa()
    {
        return $this->belongsTo(Data_Villa::class, 'villa_id');
    }
}
