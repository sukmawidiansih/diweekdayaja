<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Data_Villa;

class Gambar_Villa extends Model
{
    protected $table = 'gambar_villa';

    protected $fillable = ['id_villa', 'gambar'];

    // Perbaiki nama model dan pastikan tidak ada opsi onDelete('cascade')
    
    public function dataVilla()
    {
        return $this->belongsTo(Data_Villa::class, 'id_villa', 'id');
    }
   
}

