<?php
  
namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use Illuminate\View\View;
use App\Models\Data_Villa;
use App\Models\Gambar_Villa;
  
class PostController extends Controller
{
    //  nampilin halaman tabel data
    public function index(Request $request): View
        {
            
            // $dataVillas = Data_Villa::paginate(10);
            $search = $request->query('search');
            if(!empty($search)){
                $dataVilla = Data_Villa::where('data_villa.nama_Villa','like','%'.$search.'%')
                ->paginate(5)->onEachSide(2)->fragment('dataVilla');
            }else{
                $dataVilla = Data_Villa::paginate(5)->onEachSide(2)->fragment('dataVilla');
            }

            return view('admin.datavilla.index', compact('dataVilla','search'));
        } 
  
    // nampilin halaman form tambah data
    public function create(): View
        {
            return view('admin.datavilla.create');
        }

    // memnyimpan data ke database
    public function store(Request $request)
    {
        $request->validate([
            'Nama_Villa' => 'required',
            'Jumlah_Kamar' => 'required',
            'Max' => 'required',
            'Harga_Weekday' => 'required', // Update this to match your actual column name
            'Harga_Weekend' => 'required', // Update this to match your actual column name
            'Alamat' => 'required',
            'Deskripsi' => 'required',
            'Fasilitas' => 'required',
        ]);
    
        // Create a new instance of the Data_Villa model
        $data_villa = new Data_Villa;
    
        // Set attribute values based on the form input
        $data_villa->Nama_Villa = $request->input('Nama_Villa');
        $data_villa->Jumlah_Kamar = $request->input('Jumlah_Kamar');
        $data_villa->Max = $request->input('Max');
        $data_villa->Harga_Weekday = $request->input('Harga_Weekday');
        $data_villa->Harga_Weekend = $request->input('Harga_Weekend');
        $data_villa->Alamat = $request->input('Alamat');
        $data_villa->Deskripsi = $request->input('Deskripsi');
        $data_villa->Fasilitas = $request->input('Fasilitas');
    
        // Save the data to the database
        $data_villa->save();
    
        return redirect()->route('tabel_data')->with('success', 'Data berhasil ditambahkan!');
    }
    

    

    // edit data
    public function edit($id)
        {
            $dataVilla = Data_Villa::find($id);
            return view('admin.datavilla.edit', compact('dataVilla'));
        }
    
    // menyimpan updatan
    public function update(Request $request, $id)
        {
            $request->validate([
                'Nama_Villa' => 'required',
                'Jumlah_Kamar' => 'required',
                'Max' => 'required',
                'Harga_Weekday' => 'required',
                'Harga_Weekend' => 'required',
                'Alamat' => 'required',
                'Deskripsi' => 'required',
                'Fasilitas' => 'required',
            ]);

            // Temukan data villa berdasarkan ID
            $dataVilla = Data_Villa::findOrFail($id);

            // Update data dalam database menggunakan model
            $dataVilla->Nama_Villa = $request->input('Nama_Villa');
            $dataVilla->Jumlah_Kamar = $request->input('Jumlah_Kamar');
            $dataVilla->Max = $request->input('Max');
            $dataVilla->Harga_Weekday = $request->input('Harga_Weekday');
            $dataVilla->Harga_Weekend = $request->input('Harga_Weekend');
            $dataVilla->Alamat = $request->input('Alamat');
            $dataVilla->Deskripsi = $request->input('Deskripsi');
            $dataVilla->Fasilitas = $request->input('Fasilitas');

            // Simpan perubahan
            $dataVilla->save();

            return redirect()->route('tabel_data')->with('success', 'Data berhasil diperbarui!');
        }

    // hapus data
    public function destroy($id)
    {
        // Temukan data berdasarkan ID
        $dataVilla = Data_Villa::find($id);
    
        // Jika data ditemukan, hapus dan kembalikan ke halaman tabel_data
        if ($dataVilla) {
            $dataVilla->delete();
            return redirect()->route('tabel_data')->with('success', 'Data berhasil dihapus!');
        }
    
        // Jika data tidak ditemukan, kembalikan ke halaman tabel_data dengan pesan kesalahan
        return redirect()->route('tabel_data')->with('error', 'Data tidak ditemukan!');
    }

    // nampilin detail
    public function show($id)
    {
        $dataVilla = Data_Villa::find($id);

        if (!$dataVilla) {
            abort(404); // atau sesuaikan dengan cara penanganan kesalahan lainnya
        }

        return view('admin.datavilla.detail', compact('dataVilla'));
    }
    


// public function showForm($id)
// {
//     return view('layouts.admin.datavilla.bab.form', compact('id'));
// }


}
  
   
