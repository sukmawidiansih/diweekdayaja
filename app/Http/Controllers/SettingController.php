<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Member;

class SettingController extends Controller
{
    public function index()
    {
        $userId = Auth::id();
        $user = User::find($userId);
        return view('admin.setting.index', compact('user'));
    }

    public function update(Request $request)
{
    $user = Auth::user(); // Mengasumsikan pengguna sedang diotentikasi

    // Validasi data permintaan
    $validatedData = $request->validate([
        'name' => 'required|string|max:255',
        'email' => 'required|email|max:255',
        'phone' => 'nullable|string|max:20',
        'gender' => 'nullable|in:male,female',
        'photo' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048', // Sesuaikan ukuran file maksimum dan jenis gambar yang diizinkan
    ]);

    // Update informasi pengguna
    $user->name = $validatedData['name'];
    $user->email = $validatedData['email'];
    $user->save();

    // Update informasi member jika ada
    if ($user->member) {
        $user->member->phone = $validatedData['phone'];
        $user->member->gender = $validatedData['gender'];
        $user->member->save();
    } else {
        // Jika member tidak ada, buat member baru
        $user->member()->create([
            'phone' => $validatedData['phone'],
            'gender' => $validatedData['gender'],
        ]);
    }

    // Update foto profil pengguna
    if ($request->hasFile('photo')) {
        // Pastikan user memiliki instance Member, jika tidak buat baru
        if (!$user->member) {
            $member = new Member();
            $user->member()->save($member);
        }
    
        $imageName = $request->file('photo')->hashName();
        $request->file('photo')->move('image/', $imageName);
    
        // Perbarui kolom 'photo' di tabel 'members'
        $user->member->photo = $imageName;
        $user->member->save();
    }
    

   
    return redirect()->route('profile')->with('success', 'Profil berhasil diperbarui');
}



}
