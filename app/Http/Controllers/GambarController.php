<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\Data_Villa;
use App\Models\Gambar_Villa;

class GambarController extends Controller
{
    // public function bab($id)
    // {
    //     $dataVilla = Data_Villa::find($id);
    //     $gambarVilla = Gambar_Villa::where('id_villa', $id)->paginate(5)->onEachSide(2)->fragment('gambarVilla');
    //     $gambarVilla->withPath(route('bab', $id))->setPageName('gambar_page');

    //     return view('admin.datavilla.bab.bab', compact('dataVilla', 'gambarVilla'));
    // }
    public function bab($id)
    {
        $dataVilla = Data_Villa::find($id);
        $gambarVilla = Gambar_Villa::where('id_villa', $id)->paginate(5)->onEachSide(2)->fragment('gambarVilla');
    
        return view('admin.datavilla.bab.bab', compact('dataVilla', 'gambarVilla'));
    }
    

    public function tambah($id)
    {
        $dataVilla = Data_Villa::find($id);
        return view('admin.datavilla.bab.create-bab', compact('dataVilla'));
    }

    public function store(Request $request, $id)
    {
        $request->validate([
            'gambar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $gambarVilla = new Gambar_Villa();
        $gambarVilla->id_villa = $id;

        if ($request->hasFile('gambar')) {
            $imageName = $request->file('gambar')->hashName();
            $request->file('gambar')->move('image/', $imageName);
            $gambarVilla->gambar = $imageName;
        }

        $gambarVilla->save();

        return redirect()->route('bab', $id)->with('success', 'Data Gambar Villa berhasil ditambahkan!');
    }

    public function destroy($id)
    {
        $gambarVilla = Gambar_Villa::find($id);

        if ($gambarVilla) {
            $gambarVilla->delete();
            return redirect()->route('bab', $gambarVilla->id_villa)->with('success', 'Data berhasil dihapus!');
        }

        return redirect()->route('bab')->with('error', 'Data tidak ditemukan!');
    }

    
}
