<?php
  
namespace App\Http\Controllers;
 
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Illuminate\View\View;
use App\Models\Data_Villa;
use App\Models\Gambar_Villa;
use App\Models\Blog;
use App\Models\layanan;
use App\Models\setting;
use App\Models\Todo;
  
class TodoController extends Controller
{
    

    public function create()
    {
        // Logika untuk menampilkan formulir penambahan to-do list
        return view('admin.todo.create-todo');
    }
       
    public function store(Request $request)
    {
        $request->validate([
            'text' => 'required|string|max:255',
        ]);
    
        $todo = new Todo;
    
        // Set attribute values based on the form input
        $todo->text = $request->input('text');
       
        $todo->save();
    
        return redirect()->route('admin.home')->with('success', 'Data berhasil ditambahkan!');
    }

public function show()
{
    $todo = Todo::all();
    return view('admin.adminHome', compact('todo'));
}
    


    
}