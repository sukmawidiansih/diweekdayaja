<?php
  
namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use Illuminate\View\View;
use Illuminate\Http\Response;
use App\Models\Data_Villa;
use App\Models\Gambar_Villa;
use App\Models\Blog;
use App\Models\layanan;
use App\Models\setting;
use App\Models\kategori;
class KategoriController extends Controller
{
   
    public function kategori(Request $request): View
    {
        $search = $request->query('search');
        if(!empty($search)){
            $kategori = kategori::where('nama','like','%'.$search.'%')
            ->paginate(10)->onEachSide(2)->fragment('kategori');
        }else{
            $kategori = kategori::paginate(5)->onEachSide(2)->fragment('kategori');
        }

        return view('admin.blog.kategori', compact('kategori','search'));

    }
    
        // KategoriController.php
        public function create()
        {
            return view('admin.blog.create-kat');
        }

        public function store(Request $request)
        {
            // Validasi data formulir
            $request->validate([
                'nama_kategori' => 'required|string|max:255',
            ]);

            // Simpan data ke database menggunakan model Kategori
            Kategori::create([
                'nama_kategori' => $request->nama_kategori,
            ]);

            return redirect()->route('kategori')->with('success', 'Kategori berhasil ditambahkan.');
        }

        public function destroy($id)
        {
            // Temukan kategori berdasarkan ID
            $kategori = Kategori::findOrFail($id);

            // Hapus kategori dari database
            $kategori->delete();

            return redirect()->route('kategori')->with('success', 'Kategori berhasil dihapus.');
        }

}