<?php
  
namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use Illuminate\View\View;
use App\Models\Data_Villa;
use App\Models\Gambar_Villa;
use App\Models\blog;
use App\Models\kategori;
use Illuminate\Support\Facades\Storage;

  
class BlogController extends Controller
{
    // nampilin  halaman blog
    public function blog(Request $request): View
    {
        $search = $request->query('search');

    if (!empty($search)) {
        $blog= blog::with('kategori')
            ->where('judul', 'like', '%' . $search . '%')
            ->paginate(10)
            ->onEachSide(2)
            ->appends('search', $search)
            ->fragment('blog');
    } else {
        $blog= blog::with('kategori')
            ->paginate(5)
            ->onEachSide(2)
            ->fragment('blog');
    }
    
    $kategoriBlog = kategori::all();
        return view('admin.blog.adminBlog', compact('blog','search', 'kategoriBlog'));

    }


    // nampilin halaman tamabah blog
    public function create()
    {
        $kategoriBlog = kategori::all();
        return view('admin.Blog.createBlog', compact('kategoriBlog'));
    }
    
   
    public function store(Request $request)
    {
        // Validasi data yang diterima dari formulir
        $request->validate([
            'judul' => 'required',
            'date' => 'required',
            'deskripsi' => 'required',
            'gambar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'id_kategori' => 'required',
        ]);

        // Simpan data ke dalam database menggunakan model Blog
        $blog = new blog;
        $blog->judul = $request->input('judul');
        $blog->date = $request->input('date');
        $blog->deskripsi = $request->input('deskripsi');
        $blog->id_kategori = $request->input('id_kategori');
        // Handle unggah gambar
        if ($request->hasFile('gambar')) {
            $imageName = $request->file('gambar')->hashName();
            $request->file('gambar')->move('image/', $imageName);
            $blog->gambar = $imageName;
        }

        $blog->save();

        // Redirect atau lakukan sesuatu setelah penyimpanan berhasil
        return redirect()->route('blog')->with('success', 'Data Blog berhasil ditambahkan!');
    }
    
   
    
    
  

    // public function edit($id)
    // {
    //     $blog = blog::findOrFail($id);
    //     $kategoriBlog = kategori::all();
    //     return view('admin.Blog.editBlog', compact('blog', 'kategoriBlog'));
    // }
    
    // public function update(Request $request, $id)
    // {
    //     $request->validate([
    //         'judul' => 'required',
    //         'date' => 'required',
    //         'deskripsi' => 'required',
    //         'gambar' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
    //         'id_kategori' => 'required',
    //     ]);
    
    //     $blog = blog::findOrFail($id);
    //     $blog->judul = $request->input('judul');
    //     $blog->date = $request->input('date');
    //     $blog->deskripsi = $request->input('deskripsi');
    //     $blog->id_kategori = $request->input('id_kategori');
        
    //     if ($request->hasFile('gambar')) {
    //         $imageName = $request->file('gambar')->hashName();
    //         $request->file('gambar')->move('image/', $imageName);
    //         $blog->gambar = $imageName;
    //     }
    
    //     $blog->save();
    
    //     return redirect()->route('blog')->with('success', 'Data Blog berhasil diperbarui!');
    // }

    public function edit($id)
    {
        $blog = Blog::findOrFail($id); // Perhatikan penulisan huruf besar kecil pada "Blog"
        $kategoriBlog = kategori::all();
        return view('admin.Blog.editBlog', compact('blog', 'kategoriBlog'));
    }
    
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required',
            'date' => 'required',
            'deskripsi' => 'required',
            'gambar' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'id_kategori' => 'required',
        ]);
    
        $blog = Blog::findOrFail($id); // Perhatikan penulisan huruf besar kecil pada "Blog"
        $blog->judul = $request->input('judul');
        $blog->date = $request->input('date');
        $blog->deskripsi = $request->input('deskripsi');
        $blog->id_kategori = $request->input('id_kategori');
        
        if ($request->hasFile('gambar')) {
            $imageName = $request->file('gambar')->hashName();
            $request->file('gambar')->move('image/', $imageName);
            $blog->gambar = $imageName;
        }
    
        $blog->save();
    
        return redirect()->route('blog')->with('success', 'Data Blog berhasil diperbarui!');
    }
    
    public function detail($id)
    {
        $blog = Blog::findOrFail($id);
        return view('admin.Blog.detailBlog', compact('blog'));
    }

    public function destroy($id)
    {
        // Temukan kategori berdasarkan ID
        $blog = blog::findOrFail($id);

        // Hapus blog dari database
        $blog->delete();

        return redirect()->route('blog')->with('success', 'blog berhasil dihapus.');
    }
}