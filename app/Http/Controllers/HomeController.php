<?php
  
namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use Illuminate\View\View;
use App\Models\Todo;
use App\Models\Data_Villa;
use App\Models\blog;
use App\Models\layanan;
use App\Models\video;


  
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
  
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(): View
    {
        return view('home');
    } 
  
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

     public function adminHome(Request $request): View
     {
         $search = $request->query('search');
         
         $dataVilla = Data_Villa::count();
         $blog = blog::count();
         $vid = video::count();
 
         return view('admin.adminHome', compact('search','dataVilla','blog', 'vid'));
 
     }
    // public function adminHome(): View
    // {
    //     $todo = Todo::all();
    //     return view('admin.adminHome', compact('todo'));
    // }
  
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function managerHome(): View
    {
        return view('managerHome');
    }
}
