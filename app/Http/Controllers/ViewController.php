<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Illuminate\View\View;
use App\Models\Data_Villa;
use App\Models\Gambar_Villa;
use App\Models\Blog;
use App\Models\layanan;
use App\Models\setting;
use App\Models\Todo;
use App\Models\Kategori;
use App\Models\video;
  
class ViewController extends Controller
{
    public function index()
    {
        $dataVilla = Data_Villa::take(8)->get();
        $gambarVilla = Gambar_Villa::take(1)->get();
        $Blog = Blog::orderBy('date', 'desc')->take(4)->get();
        $vid = video::take(4)->get();
        return view('welcome', compact('dataVilla','gambarVilla','Blog', 'vid'));
    }

    public function rooms()
    {
    
        $dataVilla = Data_Villa::paginate(8);
        $gambarVilla = Gambar_Villa::take(1)->get();
       
        return view('layout.rooms', compact('dataVilla','gambarVilla'));
    }
    // public function rooms()
    // {
    
    //     $dataVilla = Data_Villa::take(8)->get();
    //     $gambarVilla = Gambar_Villa::take(1)->get();
       
    //     return view('layout.rooms', compact('dataVilla','gambarVilla'));
    // }
   
    

        public function roomSingle($id)
        {
            $vid = Video::where('villa_id', $id)->get();
            $dataVilla = Data_Villa::find($id);
            if (!$dataVilla) {
                abort(404); 
            }
            $gambarVilla = Gambar_Villa::where('id_villa', $id)->get();
            $kategori = Kategori::all();
            $Blog = Blog::orderBy('date', 'desc')->take(4)->get();
            return view('layout.roomsingle', compact('dataVilla', 'gambarVilla', 'kategori', 'Blog','vid'));
        }
       
        public function search(Request $request)
        {
            $keyword = $request->input('keyword');
            $kategori = Kategori::all();
            $Villa = Data_Villa::where('nama_villa', 'like', "%$keyword%")
                ->orWhere('deskripsi', 'like', "%$keyword%")
                ->orderBy('created_at', 'desc') 
                ->get();
            if ($Villa->count() === 1) {
                return redirect()->route('roomsingle', ['id' => $Villa->first()->id]);
            }

            return view('layout.blogsingle', compact('Villa', 'kategori', 'keyword'));
        }


        // public function blogs(Request $request)
        // {
        //     $kategori = Kategori::all();
        //     $selectedKategori = $request->get('kategori'); 
        //     if ($selectedKategori) {
        //         $Blog = Blog::with('kategori')
        //             ->whereHas('kategori', function ($query) use ($selectedKategori) {
        //                 $query->where('nama_kategori', $selectedKategori);
        //             })
        //             ->orderBy('date', 'desc')
        //             ->get();
        //     } else {
        //         $Blog = Blog::with('kategori')
        //             ->orderBy('date', 'desc')
        //             ->get();
        //     }

        //     return view('layout.blogHome', compact('Blog', 'kategori'));
        // }
        public function blogs(Request $request)
        {
            $kategori = Kategori::all();
            $selectedKategori = $request->get('kategori'); 
        
            if ($selectedKategori) {
                $blog = Blog::with('kategori')
                    ->whereHas('kategori', function ($query) use ($selectedKategori) {
                        $query->where('nama_kategori', $selectedKategori);
                    })
                    ->orderBy('date', 'desc')
                    ->paginate(10); // You can adjust the number of items per page as needed
            } else {
                $blog = Blog::with('kategori')
                    ->orderBy('date', 'desc')
                    ->paginate(10); // You can adjust the number of items per page as needed
            }
        
            return view('layout.blogHome', compact('blog', 'kategori'));
        }
        

    public function blogsingle($id)
    {
        $kategori = Kategori::all();
        $blog = Blog::where('id', '!=', $id)->orderBy('date', 'desc')->take(3)->get();
        $Blog = Blog::find($id);
        if (!$Blog) {
            abort(404);
        }
        return view('layout.blogsingle', compact('Blog', 'kategori','blog'));
    }
    
    public function contact()
    {
        return view('layout.contact');
    }

    public function about()
    {
        return view('layout.about');
    }
    public function service()
    {
        return view('layout.service');
    }
}
