<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Illuminate\View\View;
use App\Models\Data_Villa;
use App\Models\Gambar_Villa;
use App\Models\Blog;
use App\Models\layanan;
use App\Models\setting;
use App\Models\Todo;
use App\Models\Kategori;
use App\Models\video;

class VideoController extends Controller
{
    public function index(Request $request)
    {
        $search = $request->query('search');

        if (!empty($search)) {
            $vid= video::with('villa')
                ->where('judul', 'like', '%' . $search . '%')
                ->paginate(10)
                ->onEachSide(2)
                ->appends('search', $search)
                ->fragment('video');
        } else {
            $vid= video::with('villa')
                ->paginate(5)
                ->onEachSide(2)
                ->fragment('video');
        }
            return view('admin.video.index', compact('vid','search'));
    }
        // $vid = Video::with('villa')->get();
        // return view('admin.video.index', compact('vid'));
    

    public function create()
    {
        $villas = Data_Villa::all(); 
        return view('admin.video.create', compact('villas'));
    }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'villa_id' => 'required|exists:data_villa,id', 
            'media' => 'required|file|mimetypes:video/mp4,video/avi,video/mpeg,video/quicktime,video/x-msvideo,video/x-ms-wmv,image/jpeg,image/png,image/jpg,image/gif,image/svg+xml|max:20480',
        ]);
    
        // Menyimpan file
        if ($request->hasFile('media')) {
            $file = $request->file('media');
            $fileName = time().'_'.$file->getClientOriginalName(); // Membuat nama file unik
            $destinationPath = public_path('/uploads');
            $file->move($destinationPath, $fileName);
    
            // Menyimpan data ke database
            $video = new Video();
            $video->villa_id = $request->villa_id; // Sesuaikan dengan nama field di database
            $video->media = 'uploads/' . $fileName; // Simpan path file
            $video->save();
    
            return redirect()->route('videos')->with('message', 'Media berhasil ditambahkan!');
        } else {
            return redirect()->back()->withErrors(['Media tidak ditemukan.']);
        }
    }
    public function show($id)
    {
        // Mengambil data villa berdasarkan ID
        $villa = Villa::findOrFail($id);
    
        // Mengambil data video berdasarkan villa_id
        $videos = Video::where('villa_id', $id)->get();
    
        // Menampilkan tampilan dengan data villa dan video
        return view('layout.roomsingle', compact('villa', 'videos'));
    }
   
    public function destroy($id)
    {
        $vid = Video::find($id);

        if ($vid) {
            $vid->delete();
            return redirect()->route('videos', $vid->villa_id)->with('success', 'Data berhasil dihapus!');
        }

        return redirect()->route('videos')->with('error', 'Data tidak ditemukan!');
    }
    // public function store(Request $request)
    // {
    
    //     $validatedData = $request->validate([
    //     'media' => 'file|mimetypes:video/mp4,video/avi,video/mpeg,video/quicktime,video/x-msvideo,video/x-ms-wmv,image/jpeg,image/png,image/jpg,image/gif,image/svg+xml|max:20480'
    // ]);
    // $file = $request->file('media');
    // $fileName = $file->getClientOriginalName();
    // $file->move('image', $fileName);

    // $insert = new video();
    // $insert->media = $fileName; 
    // $insert->save();

    // return redirect()->route('videos');

    //     $vid = new video();

    //     if($request->hasFile('media')){
    //         $imageName = $request->file('media')->hashName();
    //         $request->file('media')->move('image/', $imageName);
    //         $vid->media = $imageName;
    //         $vid->save();

    //     }
        

    //     return redirect()->route('videos')->with('message', 'Media berhasil ditambahkan!');
    // }



   


}
