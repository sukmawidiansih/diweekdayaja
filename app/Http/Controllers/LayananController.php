<?php
  
namespace App\Http\Controllers;
 
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Illuminate\View\View;
use App\Models\Data_Villa;
use App\Models\Gambar_Villa;
use App\Models\Blog;
use App\Models\layanan;
use App\Models\setting;
  
class LayananController extends Controller
{
    // nampilin  halaman blog
    public function index(Request $request): View
    {
        $search = $request->query('search');
        if(!empty($search)){
            $layanan = layanan::where('layanan','like','%'.$search.'%')
            ->paginate(10)->onEachSide(2)->fragment('layanan');
        }else{
            $layanan = layanan::paginate(5)->onEachSide(2)->fragment('layanan');
        }

        return view('admin.layanan.layanan', compact('layanan','search'));

    }

    // nampilin halaman tamabah blog
    public function create()
    {
        return view('admin.layanan.createL');
    }
    
    // menyimpan data
    public function store(Request $request)
    {
        $request->validate([
            'layanan' => 'required',
            'deskripsi' => 'required',
            'status' => 'required|in:aktif,nonaktif',
        ]);
    
        // Create a new instance of the Layanan model
        $layanan = new Layanan;
    
        // Set attribute values based on the form input
        $layanan->layanan = $request->input('layanan');
        $layanan->deskripsi = $request->input('deskripsi');
        $layanan->status = $request->input('status');
    
        // Save the data to the database
        $layanan->save();
    
        return redirect()->route('layanan')->with('success', 'Data berhasil ditambahkan!');
    }

    public function edit($id)
    {
        $layanan = Layanan::find($id);
        return view('admin.layanan.editL', compact('layanan'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'layanan' => 'required',
            'deskripsi' => 'required',
            'status' => 'required|in:aktif,nonaktif',
        ]);

        $layanan = Layanan::find($id);
        $layanan->layanan = $request->input('layanan');
        $layanan->deskripsi = $request->input('deskripsi');
        $layanan->status = $request->input('status');
        $layanan->save();

        return redirect()->route('layanan')->with('success', 'Data berhasil diperbarui!');
    }

    // detail
    public function show($id)
    {
        $layanan = Layanan::find($id);
        return view('admin.layanan.detailL', compact('layanan'));
    }

    // hapus
    public function destroy($id)
    {
        $layanan = Layanan::find($id);
        $layanan->delete();

        return redirect()->route('layanan')->with('success', 'Layanan berhasil dihapus!');
    }

    
}