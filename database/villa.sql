-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 29, 2024 at 07:55 AM
-- Server version: 10.4.32-MariaDB
-- PHP Version: 8.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `villa`
--

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE `blog` (
  `id` int(11) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `gambar` varchar(255) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deskripsi` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `id_kategori` int(11) NOT NULL,
  `date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`id`, `judul`, `gambar`, `deleted_at`, `deskripsi`, `created_at`, `updated_at`, `id_kategori`, `date`) VALUES
(1, 'cimory dairyland puncak', 'gGW52SCc9IuUSbMiTxxq44Ya4wjUIMci0WfdxByL.jpg', NULL, '<p>g, Cimory Dairyland ini memiliki jadwal operasional yang bervariasi tergantung tempat dan wahana yang ingin dituju. Saat hari biasa atau weekdays, waktu berkunjung ke wisata Cimory dimulai pukul 08.00 WIB hingga pukul 17.00 WIB.<br><br>Untuk bagian restoran dibuka pada pukul 08.00 WIB sampai 18.00 WIB, sedangkan area shop atau toko dibuka lebih lama yaitu mulai pukul 08.00 WIB hingga pukul 19.00 WIB.<br><br>Sementara, pada akhir pekan atau weekend Cimory Dairyland dibuka lebih awal. Anda bisa berkunjung ke kawasan wisatanya pada pukul 07.00 WIB hingga pukul 17.30 WIB.<br><br>Untuk bagian restoran dibuka pukul 08.00 WIB sampai 18.30 WIB, sedangkan area toko mulai buka pada pukul 07.00 WIB hingga pukul 19.00 WIB.</p>', '2024-02-28 19:49:01', '2024-02-28 19:49:01', 1, '2024-02-27'),
(2, 'Pagi Sore Puncak', 'wlY2orHFMIiw9Q4FoYfjskKALjoLOAtBgJOz1cQh.jpg', NULL, '<p>masakan padang</p>', '2024-02-28 21:04:35', '2024-02-28 21:04:35', 2, '2024-02-29');

-- --------------------------------------------------------

--
-- Table structure for table `data_villa`
--

CREATE TABLE `data_villa` (
  `id` int(11) NOT NULL,
  `Nama_Villa` varchar(255) NOT NULL,
  `Deskripsi` text NOT NULL,
  `Jumlah_Kamar` varchar(100) NOT NULL,
  `Harga_Weekday` varchar(100) NOT NULL,
  `Harga_Weekend` varchar(100) NOT NULL,
  `Alamat` varchar(255) NOT NULL,
  `Fasilitas` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `Max` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `data_villa`
--

INSERT INTO `data_villa` (`id`, `Nama_Villa`, `Deskripsi`, `Jumlah_Kamar`, `Harga_Weekday`, `Harga_Weekend`, `Alamat`, `Fasilitas`, `created_at`, `updated_at`, `Max`) VALUES
(1, 'Villa Rara', '<p>View nya bagus banget bisa liat sunset</p>', '2', '1.300.000', '2.300.000', 'Kp. Awan megamendung', '<p>2 Kamar Tidur ( 3 Extra Bed )</p><p>3 Kamar mandi</p><p>Ruang tamu ( Televisi, Karaoke, Wifi )</p><p>Dapur (Alat Masak, Alat BBQ)</p><p>Parkiran</p><p>Kolam Renang</p>', '2024-02-28 19:42:08', '2024-02-28 19:42:08', '20 Orang'),
(2, 'Erika', '<p>bagus</p>', '7', '2.000.000', '3.500.000', 'Kp. Cisuren', '<p>7 kamar</p><p>8 kamar mandi</p><p>ruang tamu</p><p>dll</p>', '2024-02-28 21:01:58', '2024-02-28 21:01:58', '35 Orang');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) NOT NULL,
  `connection` text NOT NULL,
  `queue` text NOT NULL,
  `payload` longtext NOT NULL,
  `exception` longtext NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `gambar_villa`
--

CREATE TABLE `gambar_villa` (
  `id` int(11) NOT NULL,
  `id_villa` int(11) NOT NULL,
  `gambar` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `gambar_villa`
--

INSERT INTO `gambar_villa` (`id`, `id_villa`, `gambar`, `created_at`, `updated_at`) VALUES
(1, 1, 'KiLqnvZqVkXZOe64eR0Dfq7KeFAzME8Ka8Txgd2x.jpg', '2024-02-28 19:42:52', '2024-02-28 19:42:52'),
(2, 1, 'VmecYClSVQmInekymlRYNuAHZwspCVNanJwDDw6G.jpg', '2024-02-28 19:43:16', '2024-02-28 19:43:16'),
(3, 1, '8YsXDHL890Ophs8QCZwtZt6EAlIvmYV7LulMleJJ.jpg', '2024-02-28 19:43:37', '2024-02-28 19:43:37'),
(4, 2, 'QtLmSNijpQB8hJDYC3nuTVURYyaj6H7YF92x1aPf.jpg', '2024-02-28 21:02:22', '2024-02-28 21:02:22'),
(5, 2, 'qd0O2aNaXDKyAQReJ8BYPBZ0RZLTEIk6r4YlIhgl.jpg', '2024-02-28 21:05:30', '2024-02-28 21:05:30'),
(6, 2, '3uo7neUXOGnMV2e78vjxKPWN9gariO5mHn5CZlJB.jpg', '2024-02-28 21:05:38', '2024-02-28 21:05:38'),
(7, 2, 'hRea90AVytq0ocDRAF2GPcHUmxJVvZjuMcrOs7xw.jpg', '2024-02-28 21:05:47', '2024-02-28 21:05:47'),
(8, 2, 'MLPmBNyJPM1skFs8X934llXxWIa3MXLEAJyq1vR1.jpg', '2024-02-28 21:05:57', '2024-02-28 21:05:57');

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `id` int(11) NOT NULL,
  `nama_kategori` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id`, `nama_kategori`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Wisata', '2024-02-28 19:47:49', '2024-02-28 19:47:49', NULL),
(2, 'Restoran', '2024-02-28 21:03:54', '2024-02-28 21:03:54', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE `members` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `gender` enum('male','female') DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`id`, `user_id`, `phone`, `photo`, `gender`, `created_at`, `updated_at`) VALUES
(1, 1, '089611383733', 'ijx7G3UwUPvaPa5s37hb8gHbkT2zEiRENjYAneSl.jpg', 'female', '2024-02-28 19:33:23', '2024-02-28 19:34:24'),
(2, 1, NULL, NULL, NULL, '2024-02-28 19:33:23', '2024-02-28 19:33:23');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_reset_tokens_table', 1),
(3, '2014_10_12_100000_create_password_resets_table', 1),
(4, '2019_08_19_000000_create_failed_jobs_table', 1),
(5, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(6, '2024_02_19_041818_create_videos_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_reset_tokens`
--

CREATE TABLE `password_reset_tokens` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `token` varchar(64) NOT NULL,
  `abilities` text DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `type` tinyint(4) NOT NULL DEFAULT 0,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `type`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin User', 'admin@diweekdayaja.com', NULL, '$2y$12$f8tlcMg0RejH81toc9UcMOmNhw1jJYg2AQ67MK.QoPSLMwdZhc96q', 1, NULL, '2024-02-28 19:31:46', '2024-02-28 19:31:46'),
(2, 'Manager User', 'manager@itsolutionstuff.com', NULL, '$2y$12$BeiTJMoLQyGwvS.z8446LeTTpvZ0ooEmJOcxMl0a1J2/Ye6V32fIm', 2, NULL, '2024-02-28 19:31:46', '2024-02-28 19:31:46'),
(3, 'User', 'user@itsolutionstuff.com', NULL, '$2y$12$6Sn3kXaITapkPpkaoKZD2edemcP7NdIS9aoc1wb/75ZUAErLTbun.', 0, NULL, '2024-02-28 19:31:46', '2024-02-28 19:31:46');

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

CREATE TABLE `videos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `media` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `villa_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `videos`
--

INSERT INTO `videos` (`id`, `media`, `created_at`, `updated_at`, `villa_id`) VALUES
(1, 'uploads/1709174967_Snaptik.app_7329835717578870022.mp4', '2024-02-28 19:49:27', '2024-02-28 19:49:27', 1),
(2, 'uploads/1709179385_Snaptik.app_7291692029950676230.mp4', '2024-02-28 21:03:05', '2024-02-28 21:03:05', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_villa`
--
ALTER TABLE `data_villa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `gambar_villa`
--
ALTER TABLE `gambar_villa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `password_reset_tokens`
--
ALTER TABLE `password_reset_tokens`
  ADD PRIMARY KEY (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `data_villa`
--
ALTER TABLE `data_villa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gambar_villa`
--
ALTER TABLE `gambar_villa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `videos`
--
ALTER TABLE `videos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
