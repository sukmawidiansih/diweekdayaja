@extends('layout.app')
@section('content')

<link rel="stylesheet" href="path/to/bootstrap.css">

<style>
        /* Add your custom styles here */
        .search-form {
            margin-bottom: 20px;
        }
        .categories {
            list-style: none;
            padding: 0;
        }
        .categories li {
            margin-bottom: 10px;
        }
        .blog-item {
            margin-bottom: 20px;
        }
        .custom-button {
        border-radius: 0; /* Menghapus sudut melengkung pada tombol */
    }

    </style>

<div class="hero-wrap" style="background-image: url('{{ asset('img/home2.jpg') }}');">
        <div class="overlay"></div>
        <div class="container">
            <div class="row no-gutters slider-text d-flex align-items-end justify-content-center">
                <div class="col-md-9 ftco-animate text-center d-flex align-items-end justify-content-center">
                    <div class="text">
                        <p class="breadcrumbs mb-2" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }">
                            <span class="mr-2"><a href="/">Beranda</a></span>
                            <span class="mr-2"><a href="{{ route('rooms') }}">Penginapan</a></span>
                            <span>{{ $dataVilla->Nama_Villa }}</span>
                        </p>
                        @if($dataVilla)
                            <h1 class="mb-4 bread">{{ $dataVilla->Nama_Villa }}</h1>
                        @else
                            <h1 class="mb-4 bread">Villa Not Found</h1>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="ftco-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="row">
                        <div class="col-md-12 ftco-animate">
                            <h2 class="mb-4"></h2>
                            <div class="single-slider owl-carousel">
                                @forelse($gambarVilla as $gambar)
                                    <div class="item">
                                        <a href="rooms.html"
                                           class="img d-flex justify-content-center align-items-center"
                                           style="background-image: url('{{ asset('image/' . $gambar->gambar) }}'); height: 300px;">
                                            <!-- Isi tambahan jika diperlukan (seperti overlay, tombol, dsb.) -->
                                        </a>
                                    </div>
                                @empty
                                    <p>Gambar Villa Not Found</p>
                                @endforelse
                            </div>
                        </div>
                        <div class="col-md-12 room-single mt-4 mb-5 ftco-animate">
                            @if($dataVilla)
                                <p>{!! $dataVilla->Deskripsi !!}</p>
                                <div class="d-flex justify-content-end">
                                    <div style="text-align: left;">
                                        <a href="https://wa.me/+6281252255771" class="btn btn-success custom-button">Pesan</a>
                                    </div>
                                </div>
                                <div class="d-md-flex mt-5 mb-5">
                                    <ul class="list">
                                        <li><span>Jumlah Kamar :</span> {{ $dataVilla->Jumlah_Kamar }}</li>
                                        <li><span>Max :</span> {{ $dataVilla->Max }}</li>
                                        <li><span>Alamat :</span> {{ $dataVilla->Alamat }}</li>
                                    </ul>
                                    <ul class="list ml-md-5">
                                        <li><span>Weekday :</span> {{ $dataVilla->Harga_Weekday }}</li>
                                        <li><span>Weekend :</span> {{ $dataVilla->Harga_Weekend }}</li>
                                    </ul>
                                </div>
                                <div class="col-md-6 ftco-animate mb-5">
       
       <span>Fasilitas :</span> {!! $dataVilla->Fasilitas !!}
   @else
       <p>Data Villa Not Found</p>
   @endif
</div>

<div class="col-md-6 room-single ftco-animate mb-5 mt-4">
   <h3 class="mb-4">Lihat Video</h3>
   <div class="block-16">
       <figure>
           @foreach($vid as $v)
               <video width="300" height="450" controls>
                   <source src="{{ asset($v->media) }}" type="video/mp4">
               </video>
           @endforeach
       </figure>
   </div>
</div>
</div>

</div>
               
               </div>

            <div class="col-lg-4">
                <div class="row">
                    <div class="col-md-12 sidebar ftco-animate">
                        <div class="sidebar-box">
                        <form action="{{ route('search') }}" method="get" class="search-form" id="searchForm">
                            <div class="form-group">
                                <span class="icon fa fa-search"></span>
                                <input type="text" name="keyword" class="form-control" placeholder="cari.." value="" id="searchInput">
                            </div>
                        </form>

                        </div>
                    <!-- Categories -->
                    <div class="sidebar-box ftco-animate">
                                        <div class="categories">
                                            <h3>Kategori</h3>
                                            @foreach($kategori as $k)
                                                <li><a href="{{ route('blogs', ['kategori' => $k->nama_kategori]) }}">{{ $k->nama_kategori }}<span class="count"></span></a></li>
                                            @endforeach
                                        </div>
                                    </div>

                        <div class="sidebar-box ftco-animate">
                            <h3>Artikel</h3>
                            @foreach($Blog as $b)
                                <div class="blog-item block-21 mb-4 d-flex" style="max-width: 300px;">
                                    <a class="blog-img mr-4" style="background-image: url('{{ asset('image/'. $b->gambar) }}'); width: 100px; height: 100px;"></a>
                                    <div class="text">
                                        <h3 class="heading" style="font-size: 16px;"><a href="#">{{ $b->judul }}</a></h3>
                                        <div class="meta" style="font-size: 12px;">
                                            <div><a href="#"><span class="icon-calendar"></span> {{ $b->date }}</a></div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div> 
                </div> 
            </div> 
        </div> 
    </div> 
      </section>

      <section class="instagram pt-5">
    <div class="container-fluid">
        <div class="row no-gutters justify-content-center pb-5">
            <div class="col-md-7 text-center heading-section ftco-animate">
                <h2><span>Foto Penginapan</span></h2>
            </div>
        </div>
        <div class="row no-gutters">
            @forelse($gambarVilla as $G)
                <div class="col-sm-12 col-md ftco-animate">
                    <a href="{{ asset('image/' . $G->gambar) }}" class="insta-img image-popup" style="background-image: url('{{ asset('image/' . $G->gambar) }}');">
                        <div class="icon d-flex justify-content-center">
                            <span class="align-self-center"></span>
                        </div>
                    </a>
                </div>
            @empty
                <p>No images found</p>
            @endforelse
        </div>
    </div>
</section>


<script>
    document.addEventListener('DOMContentLoaded', function() {
        var searchInput = document.getElementById('searchInput');
        var searchForm = document.getElementById('searchForm');

        searchInput.addEventListener('input', function() {
            // Otomatis kirim formulir pencarian saat input berubah
            searchForm.submit();
        });
    });
</script>
    @endsection