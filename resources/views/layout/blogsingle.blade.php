@extends('layout.app')
@section('content')

<!-- Hero Section -->
<div class="hero-wrap" style="background-image: url('{{ asset('img/home2.jpg') }}');">
    <div class="overlay"></div>
    <div class="container">
        <div class="row no-gutters slider-text d-flex align-items-end justify-content-center">
            <div class="col-md-9 ftco-animate text-center d-flex align-items-end justify-content-center">
                <div class="text">
                    <p class="breadcrumbs mb-2">
                        <span class="mr-2"><a href="{{ url('/') }}">Beranda</a></span>
                        <span class="mr-2"><a href="{{ route('blogs') }}">Artikel</a></span>
                        <span>{{ $Blog->judul }}</span>
                    </p>
                    <h1 class="mb-4 bread">{{ $Blog->judul }}</h1>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Blog Single Content -->
<section class="ftco-section ftco-degree-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 ftco-animate order-md-last">
                <h2 class="mb-3"></h2>
                <p>
                    <img src="{{ asset('image/'. $Blog->gambar) }}" alt="" class="img-fluid" style="width: 800px; height: auto;">
                </p>
                <p>{!! $Blog->deskripsi !!}</p>
                <div class="tag-widget post-tag-container mb-5 mt-5">
                    <div class="tagcloud">
                    <a href="{{ route('blogs', ['kategori' => $Blog->kategori->nama_kategori]) }}" class="tag-cloud-link">{{ $Blog->kategori->nama_kategori }}</a>
                        <a href="#" class="tag-cloud-link">{{ $Blog->date }}</a>
                      </div>
                </div>
            </div>

            <!-- Sidebar -->
            <div class="col-lg-4 sidebar ftco-animate">
                <!-- Search Form -->
                <!-- <div class="sidebar-box">
                    <form action="#" class="search-form">
                        <div class="form-group">
                            <span class="icon fa fa-search"></span>
                            <input type="text" class="form-control" placeholder="Type a keyword and hit enter">
                        </div>
                    </form>
                </div> -->
                <div class="sidebar-box">
                        <form action="{{ route('search') }}" method="get" class="search-form" id="searchForm">
    <div class="form-group">
        <span class="icon fa fa-search"></span>
        <input type="text" name="keyword" class="form-control" placeholder="cari.." value="" id="searchInput">
    </div>
</form>
                <!-- Categories -->
                <div class="sidebar-box ftco-animate">
                    <div class="categories">
                        <h3>Kategori</h3>
                        @foreach($kategori as $k)
                            <li><a href="{{ route('blogs', ['kategori' => $k->nama_kategori]) }}">{{ $k->nama_kategori }}<span class="count"></span></a></li>
                        @endforeach
                    </div>
                </div>

                <!-- Latest Blogs -->
                <div class="sidebar-box ftco-animate">
                    <h3>Blog Terbaru</h3>
                    @if($blog->isNotEmpty())
                        @foreach($blog as $b)
                            <div class="blog-item block-21 mb-4 d-flex">
                                <a class="blog-img mr-4" style="background-image: url('{{ asset('image/'. $b->gambar) }}');"></a>
                                <div class="text">
                                    <h3 class="heading"><a href="{{ route('blogsingle', $b->id) }}">{{ $b->judul }}</a></h3>
                                    <div class="meta">
                                        <div><a href="#"><span class="icon-calendar"></span> {{ $b->date }}</a></div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @else
                        <p>Tidak ada posting blog.</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
