<style>
.icon-tiktok-circle {
    display: inline-block;
    width: 33px; /* Sesuaikan ukuran buletan sesuai kebutuhan */
    height: 33px; /* Sesuaikan ukuran buletan sesuai kebutuhan */
    border-radius: 40%;
    overflow: hidden;
    /* Ganti dengan warna latar belakang yang diinginkan */
}

.icon-tiktok-circle img {
    width: 100%;
    height: auto;
}
.ftco-footer-social.list-unstyled {
    display: flex;
    flex-wrap: nowrap; /* Tetap dalam satu baris */
}

.ftco-footer-social.list-unstyled li {
    margin-right: 10px; /* Sesuaikan jarak antar ikon */
}

  footer.ftco-footer {
    padding-top: 30px; /* Sesuaikan kebutuhan Anda */
    padding-bottom: 10px; /* Sesuaikan kebutuhan Anda */
  }

  .ftco-footer-widget h2 {
    margin-bottom: 10px; /* Sesuaikan kebutuhan Anda */
  }

  .ftco-footer-widget ul li {
    margin-bottom: 5px; /* Sesuaikan kebutuhan Anda */
  }

  .ftco-footer-widget .block-23 ul li {
    margin-bottom: 5px; /* Sesuaikan kebutuhan Anda */
  }

  .ftco-footer .text-center p {
    margin-top: 10px; /* Sesuaikan kebutuhan Anda */
    margin-bottom: 0; /* Menghilangkan margin bawah */
  }
</style>

    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-jifW828x+OVJKFI4pYa1eigtDKS7Z5Jgw5aETtQtjT5q+Cft5BB3RvFysRbyRmg7h3erW5x3Rjvw7yh5jPzRapQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />

    <footer class="ftco-footer ftco-bg-dark ftco-section">
      <div class="container"><br>
        <div class="row mb-5">
        <div class="col-md">
    <div class="ftco-footer-widget mb-4">
        <h2 class="ftco-heading-2">Di Weekday Aja</h2>
        <p>Kami menyediakan beberapa Private Villa yang dapat Anda sewa secara harian.
            Kami menyediakan area untuk family gathering, garden party, 
            pool party. Kami juga menyediakan catering, paket BBQ, Sound Sistem tambahan dan penyewaan extra bed.</p>
        <ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-5">
            <li class="ftco-animate"><a href="https://www.youtube.com/@pt.qelopakteknologiindonesia"><span class="icon-youtube"></span></a></li>
            <li class="ftco-animate"><a href="https://m.facebook.com/p/PT-Qelopak-Teknologi-Indonesia-100069960721736/?locale=id_ID"><span class="icon-facebook"></span></a></li>
            <li class="ftco-animate"><a href="https://www.instagram.com/qelopak.ti/?igshid=MzRlODBiNWFlZA%3D%3D"><span class="icon-instagram"></span></a></li>
            <!-- <li class="ftco-animate"><a href="https://id.linkedin.com/company/qelopak-teknologi-indonesia"><span class="icon-linkedin"></span></a></li> -->
            <li class="ftco-animate"><a href="https://www.google.com/"><span class="icon-google"></span></a></li>
            <li class="ftco-animate">
                <a href="https://www.tiktok.com/@di_weekday_aja?is_from_webapp=1&sender_device=pc">
                    <span class="icon-tiktok-circle">
                        <img src="{{ asset('image/tiktok.png') }}" alt="TikTok">
                    </span>
                </a>
            </li>
        </ul>
    </div>
</div>

          <div class="col-md">
            <div class="ftco-footer-widget mb-4 ml-md-5">
            <h2 class="ftco-heading-2">     </h2>
            <br>
              <ul class="list-unstyled">
                <li><a href="/" class="py-2 d-block">Beranda</a></li>
                <li><a href="{{ route('about') }}" class="py-2 d-block">Tentang Kami</a></li>
                <li><a href="{{ route('rooms') }}" class="py-2 d-block">Penginapan</a></li>
              </ul>
            </div>
          </div>
          <div class="col-md">
             <div class="ftco-footer-widget mb-4">
             <h2 class="ftco-heading-2">     </h2><br>
              <ul class="list-unstyled">
                <li><a href="{{ route('blogs') }}" class="py-2 d-block">Artikel</a></li>
                <li><a href="{{ route('service') }}" class="py-2 d-block">Layanan</a></li>
                <li><a href="{{ route('contact') }}" class="py-2 d-block">Kontak </a></li>
              </ul>
            </div>
          </div>
          <div class="col-md">
            <div class="ftco-footer-widget mb-4">
            	<h2 class="ftco-heading-2">Kontak</h2>
            	<div class="block-23 mb-3">
	              <ul>
	                <li><span class="icon icon-map-marker"></span><span class="text">Kp. Neglasari RT 05/04, Desa, Tugu Utara, Kec. Cisaru, Kota Bogor, Jawa Barat 16116</span></li>
	                <li><a href="https://wa.me/+6281252255771"></a><span class="icon icon-phone"></span><span class="text">+81252255771</span></li>
	                <li><a href="#"><span class="icon icon-envelope"></a></span><span class="text">syskmazz@gmail.com</span></li>
	              </ul>
	            </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 text-center">

            <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
  Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | diweekdayaja <i class=" color-danger" ></i>  </a>
  <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
          </div>
        </div>
      </div>
    </footer>
    
  

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>





<!-- <footer>
       
       <div class="footer-area black-bg footer-padding">
           <div class="container">
               <div class="row d-flex justify-content-between">
                   <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6">
                      <div class="single-footer-caption mb-30"> -->
                         <!-- logo -->
                         <!-- <div class="footer-logo">
                           <a href="index.html"><img src="assets/img/logo/logo2_footer.png" alt=""></a>
                         </div>
                         <div class="footer-social footer-social2">
                             <a href="https://www.instagram.com/qelopak.ti/?igshid=MzRlODBiNWFlZA%3D%3D"><i class=" ti-instagram"></i></a>
                             <a href="https://m.facebook.com/p/PT-Qelopak-Teknologi-Indonesia-100069960721736/?locale=id_ID"><i class="ti-facebook"></i></a>
                             <a href="https://id.linkedin.com/company/qelopak-teknologi-indonesia"><i class="ti-linkedin"></i></a>
                             <a href="https://www.youtube.com/@pt.qelopakteknologiindonesia"><i class="ti-youtube"></i></a>
                             <a href="https://www.qelopak.com/"><i class="ti-google"></i></a>
                         </div>
                         
                      </div>
                   </div>



                   <div class="col-xl-3 col-lg-3 col-md-3 col-sm-5">
                       <div class="single-footer-caption mb-30">
                           <div class="footer-tittle">
                               <ul><li><a href="#">Pages</a></li>
                                   <li><a href="/">Home</a></li>
                                   <li><a href="#">About</a></li>
                                   <li><a href="#">Service</a></li>
                                   
                                  
                               </ul>
                           </div>
                       </div>
                   </div>
                   <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3">
                       <div class="single-footer-caption mb-30">
                           <div class="footer-tittle">
                               
                               <ul> 
                                    <li><a href="#"></a></li><br>
                                    <li><a href="#">Blog</a></li>
                                   <li><a href="#">Contact</a></li>
                               </ul>
                           </div>
                       </div>
                   </div>
                   <div class="col-xl-3 col-lg-3 col-md-4 col-sm-5">
                        <div class="single-footer-caption mb-30">
                            <div class="footer-tittle">
                                <ul>
                                    <li><a href="#">Contact</a></li>
                                    <li>
                                        <a href="">
                                            <i class="fa fa-envelope" style="margin-right: 5px;"></i>
                                            <span>syskmazz@gmail.com</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://wa.me/+6289611383733">
                                            <i class="fa fa-phone" style="margin-right: 5px;"></i>
                                            <span>+62 89611383733</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-map-marker" style="margin-right: 5px;"></i>
                                            <span>Ruko Pakuan Regency, Jl. Amparan Jati Blok A2 No 11, RT.01/RW.07, Margajaya, Kec. Bogor bar., Kota Bogor, Jawa Barat 16116</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="footer-pera center"> -->
                        <!-- <p> -->
                        <!-- Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="ti-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                        </p>
                    </div>
               </div>
           </div>
       </div> -->
       
       <!-- Footer End-->
   <!-- </footer> -->
   