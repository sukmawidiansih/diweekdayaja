<header>
    <style>
        .navbar-brand img {
    max-width: 15%; /* Set the maximum width to 100% */
    height: auto; /* Maintain the aspect ratio */
}

/* Optionally, you can add some padding around the image */
.navbar-brand {
    padding: 3px; /* Adjust the padding as needed */
}
    </style>
<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
	    <!-- <div class="container">
	      <a class="navbar-brand" href="index.html"><img src="img/dwa.png" alt=""></a> -->
	    <div class="container">
	      <a class="navbar-brand" href="{{ url('/') }}">
    <img src="{{ asset('img/dwa.png') }}" alt="">
</a>
	      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
	        <span class="oi oi-menu"></span> Menu
	      </button>

          <div class="collapse navbar-collapse" id="ftco-nav">
    <ul class="navbar-nav ml-auto">
        <li class="nav-item {{ Request::is('/') ? 'active' : '' }}"><a href="/" class="nav-link">Beranda</a></li>
        <li class="nav-item {{ Request::is('rooms') ? 'active' : '' }}"><a href="{{ route('rooms') }}" class="nav-link">Penginapan</a></li>
        <li class="nav-item {{ Request::is('blogs') ? 'active' : '' }}"><a href="{{ route('blogs') }}" class="nav-link">Artikel</a></li>
        <li class="nav-item {{ Request::is('service') ? 'active' : '' }}"><a href="{{ route('service') }}" class="nav-link">Layanan</a></li>
        <li class="nav-item {{ Request::is('about') ? 'active' : '' }}"><a href="{{ route('about') }}" class="nav-link">Tentang Kami</a></li>
        <li class="nav-item {{ Request::is('contact') ? 'active' : '' }}"><a href="{{ route('contact') }}" class="nav-link">Kontak</a></li>
        <li class="nav-item {{ Request::is('login') ? 'active' : '' }}"><a href="{{ route('login') }}" class="nav-link">Login</a></li>
    </ul>
</div>


<script>
    $(document).ready(function () {
        // Mendapatkan path dari URL saat ini
        var path = window.location.pathname;

        // Jika path adalah root ("/"), ubah menjadi "/rooms" karena tampaknya root mengarah ke "Villa's"
        if (path === "/") {
            path = "/rooms";
        }

        // Mengaktifkan kelas "active" pada elemen menu yang sesuai
        $(".navbar-nav a").each(function () {
            var href = $(this).attr('href');

            // Menambahkan kelas "active" jika path di URL sesuai dengan href menu
            if (path === href) {
                $(this).closest('li').addClass('active');
            }
        });
    });
</script>



        <!-- Header Start -->
       <!-- <div class="header-area header-sticky">
            <div class="main-header ">
                <div class="container">
                    <div class="row align-items-center"> -->
                        <!-- logo -->
                        <!-- <div class="col-xl-2 col-lg-2">
                            <div class="logo">
                               <a href="index.html"><img src="img/dwa.png" alt=""></a>
                            </div>
                        </div>
                    <div class="col-xl-8 col-lg-8"> -->
                            <!-- main-menu -->
                            <!-- <div class="main-menu f-right d-none d-lg-block">
                                <nav>
                                    <ul id="navigation">                                                                                                                                     
                                        <li><a href="/">Home</a></li>
                                        <li><a href="#">About</a></li>
                                        <li><a href="#">Service</a></li>
                                        <li><a href="#">Blog</a>
                                        </li>
                                        <li><a href="#">Contact</a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>             
                        <div class="col-xl-2 col-lg-2"> -->
                            <!-- header-btn -->
                            <!-- <div class="header-btn">
                                <a href="{{ route('login') }}" class="btn btn1 d-none d-lg-block ">Login</a>
                            </div>
                            
                        </div> -->
                        <!-- Mobile Menu -->
                        <!-- <div class="col-12">
                            <div class="mobile_menu d-block d-lg-none"></div>
                        </div>
                    </div>
                </div>
            </div>
       </div> -->
        <!-- Header End -->
    </header>