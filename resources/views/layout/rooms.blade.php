@extends('layout.app')
@section('content')
<style>
    .pagination-success .page-item.active .page-link,
    .pagination-success .page-item.active .page-link:focus,
    .pagination-success .page-item.active .page-link:hover {
        background-color: #28a745; /* Warna hijau sukses */
        border-color: #28a745; /* Warna garis hijau sukses */
    }

    .pagination-success .page-item .page-link {
        color: #28a745; /* Warna teks hijau sukses */
    }

    .pagination-success .page-item .page-link:hover {
        color: #218838; /* Warna teks hijau gelap saat dihover */
    }
</style>

<div class="hero-wrap" style="background-image: url('{{ asset('img/home.jpg') }}');">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text d-flex align-itemd-end justify-content-center">
          <div class="col-md-9 ftco-animate text-center d-flex align-items-end justify-content-center">
          	<div class="text">
	            <p class="breadcrumbs mb-2"><span class="mr-2"><a href="/">Beranda</a></span> <span>Penginpan</span></p>
	            <h1 class="mb-4 bread">Penginapan</h1>
            </div>
          </div>
        </div>
      </div>
    </div>

	<section class="ftco-section bg-light">
    <div class="container">
        <div class="row">
            <!-- rooms.blade.php -->

@foreach($dataVilla as $villa)
    <div class="col-sm col-md-6 col-lg-4 ftco-animate">
        <div class="room">
            @php
                $gambar = $villa->gambarVilla->first();
            @endphp

            @if ($gambar)
                <a href="{{ route('roomsingle', $villa->id) }}" class="img d-flex justify-content-center align-items-center" style="background-image: url('{{ asset('image/' . $gambar->gambar) }}');">
                    <div class="icon d-flex justify-content-center align-items-center">
                        <span class="icon-search2"></span>
                    </div>
                </a>
            @endif

            <div class="text p-3 text-center">
                <h3 class="mb-3"><a href="">{{ $villa->Nama_Villa }}</a></h3>
                <ul class="list">
                    <li><span>Max:</span> {{ $villa->Max }}</li>
                    <li><span>Jumlah Kamar:</span> {{ $villa->Jumlah_Kamar }}</li>
                    <li><span>Weekday :</span> {{ $villa->Harga_Weekday }}</li>
                    <li><span>Weekend :</span> {{ $villa->Harga_Weekend }}</li>
                </ul>
                <hr>
                <p class="pt-1"><a href="{{ route('roomsingle', $villa->id) }}" class="btn-custom">Lihat Detail <span class="icon-long-arrow-right"></span></a></p>
                
            </div>
        </div>
    </div>
@endforeach

        </div>

        {{ $dataVilla->links() }}

    
</div>
</section>

    @endsection