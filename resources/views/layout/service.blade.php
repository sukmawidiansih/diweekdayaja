@extends('layout.app')
@section('content')
<div class="hero-wrap" style="background-image: url('{{ asset('img/home.jpg') }}');">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text d-flex align-itemd-end justify-content-center">
          <div class="col-md-9 ftco-animate text-center d-flex align-items-end justify-content-center">
          	<div class="text">
	            <p class="breadcrumbs mb-2"><span class="mr-2"><a href="/">Beranda</a></span> <span>Laanan</span></p>
	            <h1 class="mb-4 bread">Layanan Kami</h1>
            </div>
          </div>
        </div>
      </div>
    </div>

<section class="ftco-section">
    <div class="container">
        
        <div class="row d-flex">

            <div class="col-md-4 d-flex align-self-stretch ftco-animate">
                <div class="media block-6 services py-4 d-block text-center">
                    <div class="d-flex justify-content-center">
                        <div class="icon d-flex align-items-center justify-content-center">
                            <span class="flaticon-spa"></span>
                        </div>
                    </div>
                    <div class="media-body p-2 mt-2">
                        <h3 class="heading mb-3">Penginapan</h3>
                        <p>Kami siap membantu Anda menemukan penginapan yang sesuai dengan kebutuhan Anda. 
                         Silakan berkonsultasi dengan tim kami untuk mendapatkan penginapan yang cocok dan nyaman.</p>
                    </div>
                </div>
            </div>

            <div class="col-md-4 d-flex align-self-stretch ftco-animate">
                <div class="media block-6 services py-4 d-block text-center">
                    <div class="d-flex justify-content-center">
                        <div class="icon d-flex align-items-center justify-content-center">
                            <span class="flaticon-reception-bell"></span>
                        </div>
                    </div>
                    <div class="media-body p-2 mt-2">
                        <h3 class="heading mb-3">Catering</h3>
                        <p>Kami menyediakan layanan catering untuk menyertai Anda selama menginap. Silakan hubungi tim kami untuk mendapatkan informasi lebih lanjut mengenai layanan catering kami. 
                           </p>
                    </div>
                </div>
            </div>

            <div class="col-md-4 d-flex align-self-stretch ftco-animate">
                <div class="media block-6 services py-4 d-block text-center">
                    <div class="d-flex justify-content-center">
                        <div class="icon d-flex align-items-center justify-content-center">
                            <span class="flaticon-car"></span>
                        </div>
                    </div>
                    <div class="media-body p-2 mt-2">
                        <h3 class="heading mb-3">Transportasi</h3>
                        <p>Kami menyediakan layanan transportasi untuk penjemputan dan pengantaran bagi Anda yang ingin menginap. Untuk informasi lebih lanjut, silakan hubungi tim kami. </p>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

    @endsection