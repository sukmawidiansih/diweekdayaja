<!doctype html>
<html class="no-js" >
<head>
    <title>Diweekdayaja</title>
    
        <link rel="shortcut icon" href="{{ asset('master/images/dwa.png')}}">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"></script>

    
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700,700i" rel="stylesheet">
    

    <link rel="stylesheet" href="{{ asset('deluxe-master/css/open-iconic-bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('deluxe-master/css/animate.css')}}">
    
    <link rel="stylesheet" href="{{ asset('deluxe-master/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{ asset('deluxe-master/css/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{ asset('deluxe-master/css/magnific-popup.css')}}">

    <link rel="stylesheet" href="{{ asset('deluxe-master/css/aos.css')}}">

    <link rel="stylesheet" href="{{ asset('deluxe-master/css/ionicons.min.css')}}">

    <link rel="stylesheet" href="{{ asset('deluxe-master/css/bootstrap-datepicker.css')}}">
    <link rel="stylesheet" href="{{ asset('deluxe-master/css/jquery.timepicker.css')}}">

    
    <link rel="stylesheet" href="{{ asset('deluxe-master/css/flaticon.css')}}">
    <link rel="stylesheet" href="{{ asset('deluxe-master/css/icomoon.css')}}">
    <link rel="stylesheet" href="{{ asset('deluxe-master/css/style.css')}}">
  </head>
      
   
@include('layout.navbar')
<main>
    @yield('content')
</main>
@include('layout.footer')

       
<a href="https://wa.me/+6281252255771" target="_blank" class="whatsapp-button">
	<span class="icon-whatsapp 3rem"></span></a></li>
</a>
<style>
    .whatsapp-button {
        position: fixed;
        bottom: 20px;
        right: 20px;
        background-color: #36e453;
        border-radius: 20%;
        padding: 15px;
        display: flex;
        align-items: center;
        justify-content: center;
        text-decoration: none;
        transition: background-color 0.3s ease;
    }

    .icon-whatsapp {
        width: 750%;
        height: 30%;
		color: white;
    }

    .whatsapp-button:hover {
        background-color: #ffffff;
    }
</style>
  <script src="{{ asset('deluxe-master/js/jquery.min.js')}}"></script>
  <script src="{{ asset('deluxe-master/js/jquery-migrate-3.0.1.min.js')}}"></script>
  <script src="{{ asset('deluxe-master/js/popper.min.js')}}"></script>
  <script src="{{ asset('deluxe-master/js/bootstrap.min.js')}}"></script>
  <script src="{{ asset('deluxe-master/js/jquery.easing.1.3.js')}}"></script>
  <script src="{{ asset('deluxe-master/js/jquery.waypoints.min.js')}}"></script>
  <script src="{{ asset('deluxe-master/js/jquery.stellar.min.js')}}"></script>
  <script src="{{ asset('deluxe-master/js/owl.carousel.min.js')}}"></script>
  <script src="{{ asset('deluxe-master/js/jquery.magnific-popup.min.js')}}"></script>
  <script src="{{ asset('deluxe-master/js/aos.js')}}"></script>
  <script src="{{ asset('deluxe-master/js/jquery.animateNumber.min.js')}}"></script>
  <script src="{{ asset('deluxe-master/js/bootstrap-datepicker.js')}}"></script>
  <script src="{{ asset('deluxe-master/js/jquery.timepicker.min.js')}}"></script>
  <script src="{{ asset('deluxe-master/js/scrollax.min.js')}}"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <script src="{{ asset('deluxe-master/js/google-map.js')}}"></script>
  <script src="{{ asset('deluxe-master/js/main.js')}}"></script>
    

        
    </body>
</html>