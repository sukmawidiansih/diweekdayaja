<!-- <style>
 #left-panel .nav .active i {
    color: #28a745; /* Ganti dengan warna ijo yang diinginkan */
}
 #left-panel .nav .active a {
    color: #28a745; /* Ganti dengan warna ijo yang diinginkan */
}
</style> -->

<body>
    <!-- Left Panel -->
    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">
            <div id="main-menu" class="main-menu collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="{{ request()->routeIs('admin.home') ? 'active' : '' }}">
                    <a href="{{ route('admin.home') }}"><i class="menu-icon fa fa-laptop"></i>Dashboard</a>
                </li>
                <li class="{{ request()->routeIs('tabel_data') ? 'active' : '' }}">
                    <a href="{{ route('tabel_data') }}"><i class="menu-icon fa fa-table"></i>Data Penginapan</a>
                
                <li class="menu-item-has-children dropdown {{ request()->routeIs('blog*') ? 'active' : '' }}" id="blogDropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon pe-7s-browser"></i>Blog</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="#"></i><a href="{{ route('kategori') }}" class="dropdown-item" onclick="setActive('kategori')">Kategori Blog</a></li>
                        <li><i class="#"></i><a href="{{ route('blog') }}" class="dropdown-item" onclick="setActive('data')">Data Blog</a></li>
                    </ul>
                </li>
                </li>
                                <li class="{{ request()->routeIs('videos') ? 'active' : '' }}">
                    <a href="{{ route('videos') }}"><i class="menu-icon ti-instagram"></i>Postingan</a>
                </li>
                                <!-- <li class="{{ request()->routeIs('layanan') ? 'active' : '' }}">
                    <a href="{{ route('layanan') }}"><i class="menu-icon fa fa-glass"></i>Layanan</a>
                </li> -->
                <!-- <li class="{{ request()->routeIs('testimoni') ? 'active' : '' }}">
                    <a href="#"><i class="menu-icon ti-comment"></i>Testimoni</a>
                </li> -->
                <!-- <li class="{{ request()->routeIs('user') ? 'active' : '' }}">
                    <a href="#"><i class="menu-icon ti-user"></i>User</a>
                </li> -->
                <li class="{{ request()->routeIs('profile') ? 'active' : '' }}">
                    <a href="{{ route('profile') }}"><i class="menu-icon ti-settings"></i>Pengaturan</a>
                </li>
            </ul>

                </ul>
                
            </div><!-- /.navbar-collapse -->
        </nav>
    </aside>
    <!-- /#left-panel -->
    <script>
    function setActive(type) {
        // Hapus kelas active dari semua item dropdown
        document.querySelectorAll('#blogDropdown .dropdown-item').forEach(item => {
            item.classList.remove('active');
        });

        // Tambahkan kelas active pada item yang dipilih
        const selectedItem = document.querySelector(`#blogDropdown .dropdown-item[data-type="${type}"]`);
        if (selectedItem) {
            selectedItem.classList.add('active');
        }
    }

    // Tentukan kelas active berdasarkan halaman yang sedang diakses
    document.addEventListener('DOMContentLoaded', function() {
        const currentRoute = window.location.pathname;
        if (currentRoute.includes('kategori')) {
            setActive('kategori');
        } else if (currentRoute.includes('blog')) {
            setActive('data');
        }
    });
</script>