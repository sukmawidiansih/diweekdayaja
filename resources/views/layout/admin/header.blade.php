@php
    $user = \App\Models\User::find(Auth::id());
@endphp
<style>
.user-menu.dropdown-menu {
    width: 200px; /* Sesuaikan dengan lebar yang diinginkan */
}

.user-menu.dropdown-menu a {
    font-size: 14px; /* Sesuaikan dengan ukuran font yang diinginkan */
    padding: 8px 16px; /* Sesuaikan dengan padding yang diinginkan */
}

.user-menu.dropdown-menu i {
    margin-right: 8px; /* Sesuaikan dengan jarak antara ikon dan teks */
}
.profile-image {
        border-radius: 50%;
        width: 100px; /* Sesuaikan dengan lebar yang diinginkan */
        height: 45px; /* Sesuaikan dengan tinggi yang diinginkan */
        object-fit: cover;
    }

</style>

<div id="right-panel" class="right-panel">
        <!-- Header-->
        <header id="header" class="header">
            <div class="top-left">
                <div class="navbar-header">
                    <a class="navbar-brand" href="/"><img src="{{ asset('master/images/dwa.png')}}" alt="Logo"><span>di weekday aja</span></a>
                    <a class="navbar-brand hidden" href="/"><img src="images/logo2.png" alt="Logo"></a>
                    <a id="menuToggle" class="menutoggle"><i class="fa fa-bars"></i></a>
                </div>
            </div>
            <div class="top-right">
                <div class="header-menu">
                    <div class="header-left">
                        

                    <div class="user-area dropdown float-right">
                        
                        <a href="#" class="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img class="profile-image" src="{{ asset('image/' . $user->member->photo) }}" alt="User Avatar"  style="width: 100px; height: 45px;">

                        </a>

                        <div class="user-menu dropdown-menu">
                            <a class="nav-link" href="{{ route('profile')}}"><i class="fa fa -cog"></i>Settings</a>
                            <a class="nav-link" href="{{ route('logout')}}" onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();">
                            <i class=" text-primary"></i>
                            {{ __('Logout') }}
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form><i class="fa fa-power -off"></i></a>
                        </div>
                        
                    </div>
                </div>
            </div>
            
        </header><br>
        <!-- /#header -->