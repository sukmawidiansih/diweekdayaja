@extends('layout.app')
@section('content')
<style>
 .ftco-social .ftco-animate a span,
    .ftco-social .ftco-animate a img {
        margin: 0 2px; 
        vertical-align: middle;
    }
    .ftco-social .ftco-animate a span {
        font-size: 18px; 
        color: #000;
    }
    .ftco-social .ftco-animate a img {
        width: 18px; 
        height: 18px; 
        filter: brightness(70%); 
    }
</style>



<div class="hero-wrap" style="background-image: url('{{ asset('img/home2.jpg') }}');">
    <div class="overlay"></div>
    <div class="container">
        <div class="row no-gutters slider-text d-flex align-items-end justify-content-center">
            <div class="col-md-9 ftco-animate text-center d-flex align-items-end justify-content-center">
                <div class="text">
                    <p class="breadcrumbs mb-2"><span class="mr-2"><a href="/">Beranda</a></span> <span>Tentang kami</span></p>
                    <h1 class="mb-4 bread">Tentang Kami</h1>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="ftco-section ftc-no-pb ftc-no-pt">
    <div class="container">
        <div class="row">
            <div class="col-md-5 p-md-5 img img-2 d-flex justify-content-center align-items-center" style="background-image: url('{{ asset('img/abt.jpeg') }}');">
            </div>
            <div class="col-md-7 py-5 wrap-about pb-md-5 ftco-animate">
                <div class="heading-section heading-section-wo-line pt-md-5 pl-md-5 mb-5">
                    <div class="ml-md-0">
                        <span class="subheading">Selamat Datang </span>
                        <h2 class="mb-4">Di Weekday Aja</h2>
             
                <div class="pb-md-5">
                    <p>Kami merupakan agen reservasi penginapan berupa villa Kami menyediakan beberapa Private Villa yang dapat Anda sewa secara harian. Di area Villa, kami menyediakan area untuk family gathering, garden party, pool party. Kami juga menyediakan catering, paket BBQ, Penyewaan Sound Sistem dan penyewaan extra bed.</p>
                    <p>Selain menyediakan privat Villa kami juga menyediakan penyewaan Transportasi untuk penjemputan dan pengantaran anda.</p>
                    <ul class="ftco-social d-flex">
                        <li class="ftco-animate"><a href="https://www.facebook.com/rika.kurnia.940641"><span class="icon-facebook"></span></a></li>
                        <li class="ftco-animate"><a href="https://www.instagram.com/di_weekday_aja/"><span class="icon-instagram"></span></a></li>
                        <li class="ftco-animate"><a href="https://www.tiktok.com/@di_weekday_aja?is_from_webapp=1&sender_device=pc"><img src="{{ asset('image/tk.png') }}"></a></li>
                        <li class="ftco-animate"><a href="https://wa.me/+6281252255771"><img src="{{ asset('image/wa.png') }}"></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<br><br>
@endsection
