@extends('layout.app')
@section('content')

<div class="hero-wrap" style="background-image: url('{{ asset('img/home2.jpg') }}');">
    <div class="overlay"></div>
    <div class="container">
        <div class="row no-gutters slider-text d-flex align-itemd-end justify-content-center">
            <div class="col-md-9 ftco-animate text-center d-flex align-items-end justify-content-center">
                <div class="text">
                    <p class="breadcrumbs mb-2"><span class="mr-2"><a href="{{ url('/') }}">Beranda</a></span> <span>Artikel</span></p>
                    <h1 class="mb-4 bread">Artikel</h1>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="ftco-section">
    <div class="container-fluid">

    <div class="row mb-4">
    <div class="col-md-6 offset-md-3 d-flex justify-content-end">
        <form action="{{ route('blogs') }}" method="get" class="w-100">
            <div class="form-group mr-2">
                <label for="kategori">Pilih Kategori:</label>
                <select id="kategori" name="kategori" class="form-control">
                    <option value="">Semua Kategori</option>
                    @foreach($kategori as $kat)
                        <option value="{{ $kat->nama_kategori }}" {{ request('kategori') == $kat->nama_kategori ? 'selected' : '' }}>
                            {{ $kat->nama_kategori }}
                        </option>
                    @endforeach
                </select>
            </div>
           
        </form>
    </div>
</div>


        <div class="row d-flex" id="blogContainer">
            @foreach($blog as $b)
                <div class="col-md-3 d-flex ftco-animate blog-item mb-4" data-kategori="{{ $b->kategori->id }}">
                    <div class="blog-entry align-self-stretch">
                        <a href="{{ route('blogsingle', $b->id) }}" class="block-20" style="background-image: url('{{ asset('image/'. $b->gambar) }}'); width: 300px; height: 300px; background-size: cover; background-position: center;"></a>
                        <div class="text mt-3 d-block">
                            <h3 class="heading mt-3"><a href="{{ route('blogsingle', $b->id) }}">{{ $b->judul }}</a></h3>
                            <div class="meta mb-3">
                                <div><a href="#">{{ $b->date }}</a></div>
                                <div><a href="#">{{ $b->kategori->nama_kategori }}</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>

        <!-- <div class="row mt-5">
            <div class="col text-center">
                <div class="block-27">
                    <ul>
                        <li><a href="#">&lt;</a></li>
                        <li class="active"><span>1</span></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li><a href="#">&gt;</a></li>
                    </ul>
                </div>
            </div> -->
        </div>
      </div>
      {{ $blog->links() }}

    </div>
</div>
</section>
<script>
    document.addEventListener('DOMContentLoaded', function() {
        var kategoriSelect = document.getElementById('kategori');
        var blogContainer = document.getElementById('blogContainer');
        var blogItems = blogContainer.getElementsByClassName('blog-item');

        kategoriSelect.addEventListener('change', function() {
            var selectedKategori = kategoriSelect.value;

            for (var i = 0; i < blogItems.length; i++) {
                var blogItem = blogItems[i];
                var blogKategori = blogItem.getAttribute('data-kategori');

                if (selectedKategori === '' || selectedKategori === blogKategori) {
                    blogItem.style.display = 'block';
                } else {
                    blogItem.style.display = 'none';
                }
            }
        });

        kategoriSelect.addEventListener('change', function() {
            this.form.submit();
        });
    });
</script>



@endsection
