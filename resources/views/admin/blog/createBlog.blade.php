<!-- tambah-blog.blade.php -->
@extends('layout.admin.app')

@section('content')

<form method="POST" action="{{ route('store-blog')}}" enctype="multipart/form-data" class="form-horizontal">
    @csrf

    <div class="col-lg-12">
        <div class="card">
            <div class="card-header"><strong>Create Blog</strong></div>
            <div class="card-body card-block">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="judul" class="form-control-label">Judul</label>
                            <input type="text" name="judul" placeholder="Enter blog title" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="date" class="form-control-label">Tanggal</label>
                            <input type="date" name="date" placeholder="Enter blog title" class="form-control">
                        </div>
                    </div>
                </div>

                <!-- Contoh input select -->
                <div class="form-group">
                    <label for="id_kategori">Kategori Blog</label>
                    <select name="id_kategori" class="form-control">
                        @foreach ($kategoriBlog as $kategori)
                            <option value="{{ $kategori->id }}">{{ $kategori->nama_kategori }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="gambar" class="form-control-label">Pilih Gambar</label>
                            <div class="input-group">
                                <input type="file" name="gambar" id="gambarInput">
                            </div>
                            @error('gambar')
                            <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="preview" class="form-control-label">Gambar yang Dipilih</label>
                            <div id="preview" class="text-center">
                                <!-- Display the selected image here -->
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
    <div class="form-group">
        <label for="deskripsi" class="form-control-label">Deskripsi</label>
        <textarea name="deskripsi" id="deskripsiTextarea" class="form-control"></textarea>
    </div>
</div>

                </div>

                <div class="form-actions form-group">
                    <button type="submit" class="btn btn-success btn-sm btn-block" style="height: 40px;">Simpan</button>
                </div>
            </div>
        </div>
    </div>
</form>


<script>
    document.getElementById('gambarInput').addEventListener('change', function() {
    var preview = document.getElementById('preview');
    preview.innerHTML = '';

    var fileInput = this;
    var files = fileInput.files;

    if (files && files[0]) {
        var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
        if (!allowedExtensions.exec(files[0].name)) {
            alert('Invalid file type. Please select an image file (jpg, jpeg, png, gif).');
            fileInput.value = ''; // Clear selected file
            return;
        }

        var reader = new FileReader();
        reader.onload = function (e) {
            var img = document.createElement('img');
            img.src = e.target.result;
            img.style.maxWidth = '50%';
            img.style.marginTop = '10px';
            preview.appendChild(img);
        }
        reader.readAsDataURL(files[0]);
    }
});

</script>
@endsection
