@extends('layout.admin.app')
  
@section('content')
<form method="POST" action="{{ route('update-blog', $blog->id) }}" enctype="multipart/form-data" class="form-horizontal">
 
    @csrf
    @method('PUT') <!-- Tambahkan metode PUT untuk update -->

    <div class="col-lg-12">
        <div class="card">
            <div class="card-header"><strong>Edit Blog</strong></div>
            <div class="card-body card-block">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="judul" class="form-control-label">Judul</label>
                            <input type="text" name="judul" value="{{ $blog->judul }}" placeholder="Enter blog title" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="date" class="form-control-label">Tanggal</label>
                            <input type="date" name="date" value="{{ $blog->date }}" placeholder="Enter blog title" class="form-control">
                        </div>
                    </div>
                </div>

                <!-- Contoh input select -->
                <div class="form-group">
                    <label for="id_kategori">Kategori Blog</label>
                    <select name="id_kategori" class="form-control">
                        @foreach ($kategoriBlog as $kategori)
                            <option value="{{ $kategori->id }}" {{ $blog->id_kategori == $kategori->id ? 'selected' : '' }}>
                                {{ $kategori->nama_kategori }}
                            </option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="gambar" class="form-control-label">Pilih Gambar</label>
                    <div class="input-group">
                        <input type="file" name="gambar" id="gambarInput" class="form-control" style="display: none;">
                        <input type="text" class="form-control" id="gambarFileName" readonly value="{{ $blog->gambar }}">
                        <div class="input-group-append">
                            <label class="btn btn-secondary" for="gambarInput" style="border-left: 1px solid #ced4da;">
                                Pilih
                            </label>
                        </div>
                    </div>
                    @error('gambar')
                        <small class="text-danger">{{ $message }}</small>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="preview" class="form-control-label">Gambar yang Dipilih</label>
                    <div id="preview" class="text-center">
                        <!-- Display the selected image here -->
                        <img src="{{ asset('storage/' . $blog->gambar) }}" alt="Selected Image" style="max-width: 50%;">
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="deskripsi" class="form-control-label">Deskripsi</label>
                            <textarea name="deskripsi" id="deskripsiTextarea" rows="9" placeholder="Enter blog description" class="form-control">{{ $blog->deskripsi }}</textarea>
                        </div>
                    </div>
                </div>

                <div class="form-actions form-group">
                    <button type="submit" class="btn btn-success btn-sm btn-block" style="height: 40px;">Simpan Perubahan</button>
                </div>
            </div>
        </div>
    </div>
</form>

<script>
    document.getElementById('gambarInput').addEventListener('change', function() {
        var fileName = this.value.split('\\').pop();
        document.getElementById('gambarFileName').value = fileName;

        // Display the selected image
        var preview = document.getElementById('preview');
        preview.innerHTML = ''; // Clear previous preview

        var fileInput = this;
        var files = fileInput.files;

        if (files && files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                var img = document.createElement('img');
                img.src = e.target.result;
                img.style.maxWidth = '50%';
                preview.appendChild(img);
            }
            reader.readAsDataURL(files[0]);
        }
    });
</script>
@endsection
