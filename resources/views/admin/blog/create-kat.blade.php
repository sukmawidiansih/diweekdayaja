<!-- tambah-gambar.blade.php -->
@extends('layout.admin.app')

@section('content')
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <strong>Tambah Nama Kategori</strong>
            </div>
            <div class="card-body card-block">
                <form method="POST" action="{{ route('storekat') }}" class="form-horizontal">
                    @csrf

                    <div class="form-group">
                        <label for="nama_kategori" class="form-control-label">Nama Kategori</label>
                        <input type="text" name="nama_kategori" class="form-control">
                        @error('nama_kategori')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>

                    <div class="form-actions form-group">
                        <button type="submit" class="btn btn-success btn-sm btn-block">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
