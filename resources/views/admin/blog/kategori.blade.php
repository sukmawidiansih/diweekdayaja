@extends('layout.admin.app')

@section('content')
<style>
    .center-td {
        text-align: center;
        vertical-align: middle !important;
    }
</style>
<div class="content">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-6">
                                <strong class="card-title">Tabel Kategori</strong>
                            </div>
                            <div class="col-md-6">
                                <div class="float-right">
                                    <a href="{{ route('create-kat') }}" class="btn btn-outline-success">Tambah</a>
                                </div>
                                <form action="#" method="GET" class="form-inline">
                                <div class="form-group mx-sm-3">
                                    <input type="text" name="search"  value="{{ $search }}" class="form-control" placeholder="Cari...">
                                </div>
                                <button type="submit" class="btn btn-primary">Cari</button>
                            </form>
                            </div>
                        </div>
                    </div><br>

                    <div class="table-responsive" style="margin: 15px 0; padding: 15px; background-color: #fff;">
                        <table class="table table-bordered ">
                            <thead>
                                <tr>
                                    <th class="center-td">Nomor</th>
                                    <th class="center-td">Nama</th>
                                    <th class="center-td">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                            @php
                            $no = 1 + (( $kategori->currentPage() - 1) * $kategori->perPage());
                            @endphp
                                @foreach($kategori as $k)
                                <tr>
                                    <td class="center-td">{{ $no++ }}</td>
                                    <td class="center-td">{{ $k->nama_kategori }}</td>
                                    <td class="center-td">
                                        <a href="#" onclick="event.preventDefault(); if(confirm('Apakah Anda yakin ingin menghapus?')) {document.getElementById('delete-form-{{$k->id}}').submit();}" class="btn btn-outline-danger btn-sm">
                                            <i class="fa fa-trash-o"></i>
                                        </a>
                                        <form action="{{ route('delete-kat', $k->id) }}" id="delete-form-{{$k->id}}" method="post">
                                            @csrf
                                            @method('DELETE')
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $kategori->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div><!-- .animated -->
</div><!-- .content -->

@endsection
