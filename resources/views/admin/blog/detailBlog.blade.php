@extends('layout.admin.app')

@section('content')
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header"><strong>Detail Blog</strong></div>
            <div class="card-body card-block">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="judul" class="form-control-label">Judul</label>
                            <p>{{ $blog->judul }}</p>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="judul" class="form-control-label">Tanggal</label>
                            <p>{{ $blog->date }}</p>
                        </div>
                    </div>
                  
                </div>

                <div class="form-group">
                    <label for="gambar" class="form-control-label">Gambar</label>
                    <img src="{{ asset('image/' . $blog->gambar) }}" alt="Blog Image" style="max-width: 100%;">
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="deskripsi" class="form-control-label">Deskripsi</label>
                            <p>{{ $blog->deskripsi }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    
@endsection
