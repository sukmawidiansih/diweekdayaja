@extends('layout.admin.app')

@section('content')
<style>
    .center-td {
        text-align: center;
        vertical-align: middle !important;
    }
</style>
<div class="content">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-6">
                                <strong class="card-title">Tabel Blog</strong>
                            </div>
                            <div class="col-md-6">
                                <div class="float-right">
                                    <a href="{{ route('create-blog') }}" class="btn btn-outline-success">Tambah</a>
                                </div>
                                <form action="#" method="GET" class="form-inline">
                                    <div class="form-group mx-sm-3">
                                        <input type="text" name="search"  value="{{ $search }}" class="form-control" placeholder="Cari...">
                                    </div>
                                    <button type="submit" class="btn btn-primary">Cari</button>
                                </form>
                            </div>
                        </div>
                    </div><br>

                    <div class="table-responsive" style="margin: 15px 0; padding: 15px; background-color: ffffff;">
                        <table id="bootstrap-data-table" class="table table-bordered dataTable no-footer" role="grid" aria-describedby="bootstrap-data-table_info">
                            <thead>
                                <tr>
                                    <th class="center-td">Nomor</th>
                                    <th class="center-td">Judul</th>
                                    <th class="center-td">Gambar</th>
                                    <th class="center-td">Deskripsi</th>
                                    <th class="center-td">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                $no = 1 + (( $blog->currentPage() - 1) * $blog->perPage());
                                @endphp
                                @foreach($blog as $b)
                                <tr>
                                    <td class="center-td">{{ $no++ }}</td>
                                    <td class="center-td">{{ $b->judul }}</td>
                                    <td class="center-td">
                                        @if($b->gambar)
                                            <img src="{{ asset('image/'. $b->gambar) }}" style="max-width: 25px;">
                                        @else
                                            Tidak ada gambar
                                        @endif
                                    </td>
                                    <td>{!! $b->deskripsi !!}</td>
                                    <td style="text-align: center;"> 
                                        <a href="#" class="btn btn-outline-success btn-sm" data-toggle="modal" data-target="#detailBlogModal{{ $b->id }}"><i class="fa fa-eye"></i></a>
                                        <a href="#" class="btn btn-outline-warning btn-sm" data-toggle="modal" data-target="#editBlogModal{{ $b->id }}"><i class="fa fa-pencil"></i></a>
                                        <a href="#" onclick="event.preventDefault(); if(confirm('Apakah Anda yakin ingin menghapus?')) {document.getElementById('delete-form-{{$b->id}}').submit();}" class="btn btn-outline-danger btn-sm">
                                            <i class="fa fa-trash-o"></i>
                                        </a>
                                        <form action="{{ route('destroy-blog', ['id' => $b->id]) }}" id="delete-form-{{$b->id}}" method="post">
                                            @csrf
                                            @method('DELETE')
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $blog->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div><!-- .animated -->
</div><!-- .content -->

@foreach($blog as $b)
<!-- Detail Blog Modal -->
<div class="modal fade" id="detailBlogModal{{ $b->id }}" tabindex="-1" role="dialog" aria-labelledby="detailBlogModalLabel{{ $b->id }}" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="detailBlogModalLabel{{ $b->id }}">Detail Blog</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="judul" class="form-control-label">Judul</label>
                            <p>{{ $b->judul }}</p>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="tanggal" class="form-control-label">Tanggal</label>
                            <p>{{ $b->date }}</p>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="kategori" class="form-control-label">Kategori</label>
                            <p>{{ $b->kategori->nama_kategori }}</p>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="gambar" class="form-control-label">Gambar</label><br>
                    <img src="{{ asset('image/' . $b->gambar) }}" alt="Blog Image" style="max-width: 50%;">
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="deskripsi" class="form-control-label">Deskripsi</label>
                            <p>{!! $b->deskripsi !!}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Edit Blog Modal -->
<div class="modal fade" id="editBlogModal{{ $b->id }}" tabindex="-1" role="dialog" aria-labelledby="editBlogModalLabel{{ $b->id }}" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editBlogModalLabel{{ $b->id }}">Edit Blog</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" action="{{ route('update-blog', $b->id) }}" enctype="multipart/form-data">
                    @csrf
                    @method('PATCH')
                    <div class="form-group">
                        <label for="judul" class="form-control-label">Judul</label>
                        <input type="text" name="judul" class="form-control" value="{{ $b->judul }}">
                    </div>
                    <div class="form-group">
                        <label for="date" class="form-control-label">Tanggal</label>
                        <input type="date" name="date" class="form-control" value="{{ $b->date }}">
                    </div>
                    <div class="form-group">
                        <label for="id_kategori" class="form-control-label">Kategori</label>
                        <select name="id_kategori" id="id_kategori" class="form-control">
                            @foreach ($kategoriBlog as $kategori)
                                <option value="{{ $kategori->id }}" @if($b->id_kategori == $kategori->id) selected @endif>{{ $kategori->nama_kategori }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="gambar" class="form-control-label">Gambar</label>
                        <input type="file" name="gambar" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="deskripsi" class="form-control-label">Deskripsi</label>
                        <textarea name="deskripsi" id="deskripsiTextarea" class="form-control">{{ $b->deskripsi }}</textarea>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-outline-success">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endforeach

<!-- Tutup div untuk blok utama -->
</div><!-- .animated -->
</div><!-- .content -->
@endsection
