@extends('layout.admin.app')

@section('content')
<div class="col-lg-12">
    <div class="card">
        <div class="card-header"><strong>Detail Villa</strong></div>
        <div class="card-body card-block">
            <div class="row">
                <div class="col-md-6">
                    <p><strong>Nama Penginapan:</strong> {{ $dataVilla->Nama_Villa }}</p>
                    <p><strong>Jumlah Kamar:</strong> {{ $dataVilla->Jumlah_Kamar }}</p>
                    <p><strong>Max:</strong> {{ $dataVilla->Max }}</p>
                    <p><strong>Fasilitas:</strong>{!! $dataVilla->Fasilitas !!}</p>
                    <p><strong>Harga Weekday:</strong> {{ $dataVilla->Harga_Weekday }}</p>
                    <p><strong>Harga Weekend:</strong> {{ $dataVilla->Harga_Weekend }}</p>
                    <p><strong>Deskripsi:</strong></p>
                    {!! $dataVilla->Deskripsi !!}
                    <p><strong>Alamat:</strong> {{ $dataVilla->Alamat }}</p>
                </div>
            </div>

            <a href="{{ route('tabel_data') }}" class="btn btn-success mt-3">Kembali ke Daftar Data Villa</a>
        </div>
    </div>
</div>

@endsection
