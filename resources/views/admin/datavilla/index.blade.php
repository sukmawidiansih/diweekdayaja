@extends('layout.admin.app')

@section('content')
<style>
    .center-td {
        text-align: center;
        vertical-align: middle !important;
    }
</style>

<div class="content">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-6">
                                <strong class="card-title">Tabel Data</strong>
                            </div>
                            <div class="col-md-6">
                                <div class="float-right">
                                    <a href="{{ route('create') }}" class="btn btn-outline-success">Tambah</a>
                                </div>
                                <form action="#" method="GET" class="form-inline">
                                    <div class="form-group mx-sm-3">
                                        <input type="text" name="search" value="{{ $search }}" class="form-control" placeholder="Cari...">
                                    </div>
                                    <button type="submit" class="btn btn-primary">Cari</button>
                                </form>
                            </div>
                        </div>
                    </div><br>

                    <div class="table-responsive" style="margin: 15px 0; padding: 15px; background-color: ffffff;">
                        <table id="bootstrap-data-table" class="table table-bordered dataTable no-footer" role="grid" aria-describedby="bootstrap-data-table_info">
                            <thead>
                                <tr>
                                    <th class="center-td">Nomor</th>
                                    <th class="center-td">Nama Penginapan</th>
                                    <th class="center-td">Max</th>
                                    <th class="center-td">Deskripsi</th>
                                    <th class="center-td">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                $no = 1 + (( $dataVilla->currentPage() - 1) * $dataVilla->perPage());
                                @endphp
                                @foreach($dataVilla as $d)
                                <tr>
                                    <td class="center-td">{{ $no++ }}</td>
                                    <td class="center-td">{{ $d->Nama_Villa }}</td>
                                    <td class="center-td">{{ $d->Max }}</td>
                                    <td class="center-td">{!! $d->Deskripsi !!}</td>
                                    <td class="center-td">
                                        <a href="{{ route('bab', $d->id) }}" class="btn btn-outline-primary btn-sm"><i class="fa fa-picture-o"></i></a>
                                        <a href="#" class="btn btn-outline-success btn-sm" data-toggle="modal" data-target="#viewDetailModal{{ $d->id }}">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                        <a href="#" class="btn btn-outline-warning btn-sm" data-toggle="modal" data-target="#editDataModal{{ $d->id }}">
        <i class="fa fa-pencil"></i>
    </a>
                                        <a href="{{ route('destroy', $d->id) }}" onclick="event.preventDefault(); if(confirm('Apakah Anda yakin ingin menghapus?')) {document.getElementById('delete-form-{{$d->id}}').submit();}" class="btn btn-outline-danger btn-sm">
                                            <i class="fa fa-trash-o"></i>
                                        </a>
                                        <form action="{{ route('destroy', ['id' => $d->id]) }}" id="delete-form-{{$d->id}}" method="post">
                                            @csrf
                                            @method('DELETE')
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $dataVilla->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div><!-- .animated -->
</div><!-- .content -->

@foreach($dataVilla as $d)
    <!-- Modals -->
    <div class="modal fade" id="viewDetailModal{{ $d->id }}" tabindex="-1" role="dialog" aria-labelledby="viewDetailModalLabel{{ $d->id }}" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="viewDetailModalLabel{{ $d->id }}">Detail Penginapan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <!-- Isi Detail Villa -->
                    <p><strong>Nama Penginapan:</strong> {{ $d->Nama_Villa }}</p>
                    <p><strong>Jumlah Kamar:</strong> {{ $d->Jumlah_Kamar }}</p>
                    <p><strong>Max:</strong> {{ $d->Max }}</p>
                    <p><strong>Harga Weekday:</strong> {{ $d->Harga_Weekday }}</p>
                    <p><strong>Harga Weekend:</strong> {{ $d->Harga_Weekend }}</p>
                    <p><strong>Fasilitas:</strong>{!! $d->Fasilitas !!}</p>
                    <p><strong>Deskripsi:</strong></p>
                    {!! $d->Deskripsi !!}
                    <p><strong>Alamat:</strong> {{ $d->Alamat }}</p>
                    <!-- ... (tambahkan detail lainnya) ... -->
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Edit -->
    <div class="modal fade" id="editDataModal{{ $d->id }}" tabindex="-1" role="dialog" aria-labelledby="editDataModalLabel{{ $d->id }}" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editDataModalLabel{{ $d->id }}">Edit Data Villa</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <!-- Isi form edit data -->
                <form action="{{ route('update', $d->id) }}" method="POST">
    @csrf
    @method('PATCH')

                    <div class="form-group">
                        <label for="Nama_Villa">Nama Penginapan:</label>
                        <input type="text" class="form-control" id="Nama_Villa" name="Nama_Villa" value="{{ $d->Nama_Villa }}">
                    </div>

                    <div class="form-group">
                        <label for="Jumlah_Kamar">Jumlah Kamar:</label>
                        <input type="text" class="form-control" id="Jumlah_Kamar" name="Jumlah_Kamar" value="{{ $d->Jumlah_Kamar }}">
                    </div>

                    <div class="form-group">
                        <label for="Max">Max:</label>
                        <input type="text" class="form-control" id="Max" name="Max" value="{{ $d->Max }}">
                    </div>

                    <div class="form-group">
                        <label for="Harga_Weekday">Harga Weekday:</label>
                        <input type="text" class="form-control" id="Harga_Weekday" name="Harga_Weekday" value="{{ $d->Harga_Weekday }}">
                    </div>

                    <div class="form-group">
                        <label for="Harga_Weekend">Harga Weekend:</label>
                        <input type="text" class="form-control" id="Harga_Weekend" name="Harga_Weekend" value="{{ $d->Harga_Weekend }}">
                    </div>

                    <div class="form-group">
                        <label for="Alamat">Alamat:</label>
                        <input type="text" class="form-control" id="Alamat" name="Alamat" value="{{ $d->Alamat }}">
                    </div>

                    <div class="form-group">
                        <label for="Fasilitas">Fasilitas:</label>
                        <textarea name="Fasilitas" id="fasilitasTextarea" rows="9" class="form-control">{{ $d->Fasilitas }}</textarea>
                    </div>

                    <div class="form-group">
                        <label for="Deskripsi">Deskripsi:</label>
                        <textarea name="Deskripsi" id="deskripsiTextarea" rows="9" class="form-control">{{ $d->Deskripsi }}</textarea>
                    </div>

                    <input type="hidden" name="_method" value="PATCH">
    <button type="submit"class="btn btn-success" >Simpan Perubahan</button>
</form>
            </div>
        </div>
    </div>
</div>

@endforeach

@endsection
