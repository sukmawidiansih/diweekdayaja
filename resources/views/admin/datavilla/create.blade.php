@extends('layout.admin.app')

@section('content')
    <form method="POST" action="{{ route('store')}}" enctype="multipart/form-data" class="form-horizontal">
        @csrf
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <strong>Create Data</strong>
                </div>
                <div class="card-body card-block">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="Nama_Villa" class="form-control-label">Nama Penginapan</label>
                                <input type="text" name="Nama_Villa" placeholder="Enter the villa name" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="Jumlah_Kamar" class="form-control-label">Jumlah Kamar</label>
                                <input type="text" name="Jumlah_Kamar" placeholder="Enter the room quantity" class="form-control">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="Max" class="form-control-label">Max</label>
                                <input type="text" name="Max" placeholder="Enter the maximum capacity" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="Harga_Weekday" class="form-control-label">Harga Weekday</label>
                                <input type="text" name="Harga_Weekday" placeholder="Enter weekday price" class="form-control">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="Harga_Weekend" class="form-control-label">Harga Weekend</label>
                                <input type="text" name="Harga_Weekend" placeholder="Enter weekend price" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="Alamat" class="form-control-label">Alamat</label>
                                <input type="text" name="Alamat" placeholder="Enter the address" class="form-control">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="Fasilitas" class="form-control-label">Fasilitas</label>
                                <textarea name="Fasilitas" id="fasilitasTextarea" rows="9" placeholder="Enter facilities..." class="form-control"></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="Deskripsi" class="form-control-label">Deskripsi</label>
                                <textarea name="Deskripsi" id="deskripsiTextarea" rows="9" placeholder="Enter description..." class="form-control"></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="form-actions form-group">
                        <button type="submit" class="btn btn-success btn-sm btn-block" style="height: 40px;">Simpan</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('scripts')
    <script>
        ClassicEditor
            .create( document.querySelector( '#deskripsiTextarea' ) )
            .catch( error => {
                console.error( error );
            } );
    </script>
@endsection
