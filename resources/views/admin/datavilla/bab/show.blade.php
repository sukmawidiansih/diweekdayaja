@extends('layout.admin.app')

@section('content')
    <div class="container">
        <h1>Detail Gambar Penginapan</h1>

        <div>
            <img src="{{ asset('uploads/' . $gambarVilla->gambar) }}" alt="Gambar Villa">
            <p>ID: {{ $gambarVilla->id }}</p>
            <!-- Tambahkan informasi lain sesuai kebutuhan -->
        </div>
    </div>
@endsection