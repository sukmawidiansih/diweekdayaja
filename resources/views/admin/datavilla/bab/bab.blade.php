@extends('layout.admin.app')

@section('content')
<style>
    .thumbnail {
        max-width: 200px; /* Atur lebar maksimum gambar */
        max-height: 150px; /* Atur tinggi maksimum gambar */
    }

    .center-td {
        text-align: center;
        vertical-align: middle !important;
    }

    .actions-td {
        text-align: center;
        white-space: nowrap;
    }

    .btn-danger {
        color: #fff;
        background-color: #dc3545;
        border-color: #dc3545;
    }
</style>

<div class="content">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-6">
                                <strong class="card-title">Gambar Penginapan</strong>
                            </div>
                            <div class="col-md-6">
                                <div class="float-right">
                                    <a href="{{ route('tambah', ['id' => optional($dataVilla)->id]) }}" class="btn btn-outline-success">Tambah</a>
                                </div>
                            </div>
                        </div>
                    </div><br>

                    <div class="table-responsive" style="margin: 15px 0; padding: 15px; background-color: #fff;">
                        <table class="table table-bordered ">
                            <thead>
                                <tr>
                                    <th class="center-td">Nomor</th>
                                    <th class="center-td">Gambar</th>
                                    <th class="center-td">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $no = 1 + (($gambarVilla->currentPage() - 1) * $gambarVilla->perPage());
                                @endphp
                                @foreach($gambarVilla as $d)
                                    <tr>
                                        <td class="center-td">{{ $no++ }}</td>
                                        <td class="center-td"><img src="{{ asset('image/' .$d->gambar) }}" class="thumbnail" alt="Gambar Villa"></td>
                                        <td class="center-td">
                                            <div class="d-flex justify-content-center">
                                                <a href="{{ route('destroyImg', $d->id) }}" onclick="event.preventDefault(); if(confirm('Apakah Anda yakin ingin menghapus?')) {document.getElementById('delete-form-{{$d->id}}').submit();}" class="btn btn-outline-danger btn-sm">
                                                    <i class="fa fa-trash-o"></i>
                                                </a>
                                                <form action="{{ route('destroyImg', ['id' => $d->id]) }}" id="delete-form-{{$d->id}}" method="post">
                                                    @csrf
                                                    @method('DELETE')
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $gambarVilla->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div><!-- .animated -->
</div><!-- .content -->

@endsection
