<!-- tambah-gambar.blade.php -->
@extends('layout.admin.app')

@section('content')

<style>
    .thumbnail {
        max-width: 100%;
        max-height: 300px;
        margin-top: 10px;
    }
</style>
<div class="col-lg-12">
    <div class="card">
        <div class="card-header">
            <strong>Tambah Gambar Penginapan</strong>
        </div>
        <div class="card-body card-block">
            <form method="POST" action="{{ route('storeimg', $dataVilla->id) }}" enctype="multipart/form-data" class="form-horizontal">
                @csrf

                <div class="form-group">
                    <label for="gambar" class="form-control-label">Pilih Gambar</label>
                    <div class="input-group">
                        <input type="file" name="gambar" id="gambarInput">
                    </div>
                    @error('gambar')
                    <small class="text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="preview" class="form-control-label">Gambar yang Dipilih</label>
                    <div id="preview" class="text-center">
                        <!-- Display the selected image here -->
                    </div>
                </div>

                <div class="form-actions form-group">
                    <button type="submit" class="btn btn-success btn-sm btn-block">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    document.getElementById('gambarInput').addEventListener('change', function() {
        var fileName = this.value.split('\\').pop();
        // Display the selected image
        var preview = document.getElementById('preview');
        preview.innerHTML = ''; // Clear previous preview

        var fileInput = this;
        var files = fileInput.files;

        if (files && files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                var img = document.createElement('img');
                img.src = e.target.result;
                img.className = 'thumbnail';
                preview.appendChild(img);
            }
            reader.readAsDataURL(files[0]);
        }
    });
</script>
@endsection
