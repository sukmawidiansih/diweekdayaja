@extends('layout.admin.app')

@section('content')
    <form method="POST" action="{{ route('update', $dataVilla->id) }}" enctype="multipart/form-data" class="form-horizontal">
        @csrf
        @method('PATCH')

    

        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <strong>Edit Data</strong>
                </div>
                <div class="card-body card-block">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="Nama_Villa" class="form-control-label">Nama Penginapan</label>
                                <input type="text" name="Nama_Villa" value="{{ $dataVilla->Nama_Villa }}" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="Jumlah_Kamar" class="form-control-label">Jumlah Kamar</label>
                                <input type="text" name="Jumlah_Kamar" value="{{ $dataVilla->Jumlah_Kamar }}" class="form-control">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="Max" class="form-control-label">Max</label>
                                <input type="text" name="Max" value="{{ $dataVilla->Max }}" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="Harga_Weekday" class="form-control-label">Harga Weekday</label>
                                <input type="text" name="Harga_Weekday" value="{{ $dataVilla->Harga_Weekday }}" class="form-control">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="Harga_Weekend" class="form-control-label">Harga Weekend</label>
                                <input type="text" name="Harga_Weekend" value="{{ $dataVilla->Harga_Weekend }}" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="Alamat" class="form-control-label">Alamat</label>
                                <input type="text" name="Alamat" value="{{ $dataVilla->Alamat }}" class="form-control">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="Fasilitas" class="form-control-label">Fasilitas</label>
                                <textarea name="Fasilitas" id="fasilitasTextarea" rows="9" class="form-control">{{ $dataVilla->Fasilitas }}</textarea>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="Deskripsi" class="form-control-label">Deskripsi</label>
                                <textarea name="Deskripsi" id="deskripsiTextarea" rows="9" class="form-control">{{ $dataVilla->Deskripsi }}</textarea>
                            </div>
                        </div>
                    </div>

                    <div class="form-actions form-group">
                        <button type="submit" class="btn btn-success btn-sm btn-block" style="height: 40px;">Simpan</button>
                    </div>
                </div>
            </div>
        </div> 
    </form>
@endsection
