@extends('layout.admin.app')

@section('content')
    <form method="POST" action="{{ route('todos.store') }}" enctype="multipart/form-data" class="form-horizontal">
        @csrf
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <strong>Create Data</strong>
                </div>
                <div class="card-body card-block">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="text" class="form-control-label">Text</label>
                                <textarea name="text" id="deskripsiTextarea" rows="9" placeholder="Content..." class="form-control"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions form-group">
                        <button type="submit" class="btn btn-success btn-sm btn-block" style="height: 40px;">Simpan</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection
