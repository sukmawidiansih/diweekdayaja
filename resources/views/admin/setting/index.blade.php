@extends('layout.admin.app')
@section('content')
<style>
    .profile-image {
        border-radius: 50%;
        width: 120px; /* Sesuaikan dengan ukuran yang diinginkan */
        height: 150px; /* Sesuaikan dengan ukuran yang diinginkan */
        object-fit: cover;
    }
</style>
<div class="row">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
        <div class="card-header"><strong>Pengaturan</strong></div>
        <div class="card-body card-block">
            <div class="card-body">
              <form class="forms-sample" action="{{ route('admin.setting.update') }}" method="POST" enctype="multipart/form-data"> 
                    @csrf
                    @method('PATCH')
                    <input type="hidden" name="user_id" value="{{ $user->id }}">
                    <div class="form-group text-center">
                        <label for="photo" class="d-block">Photo Profile</label>
                        <input name="photo" type="file" class="form-control-file" id="photo" onchange="previewImage(event)">
                        <div class="photo-preview">
                        @if($user->member && $user->member->photo)
                            <img src="{{ asset('image/' . $user->member->photo) }}" class="profile-image">
                        @else
                            <img src="#" alt="Default Avatar" id="preview" class="preview-image profile-image">
                        @endif
                    </div>
                    </div>
                    <div class="form-group">
                        <label for="name">Nama</label>
                        <input name="name" type="text" class="form-control" id="name" placeholder="Enter your name" value="{{ $user->name ?? '' }}">
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input name="email" type="email" class="form-control" id="email" placeholder="Enter your email" value="{{ $user->email ?? '' }}">
                    </div>
                    <div class="form-group">
                        <label for="phone">No Telpon</label>
                        <input name="phone" type="text" class="form-control" id="phone" placeholder="Enter your phone" value="{{ $user->member->phone ?? '' }}">
                    </div>
                    <div class="form-group">
                        <label for="gender">Jenis Kelamin</label>
                        <select name="gender" class="form-control" id="gender">
                            <option value="male" {{ $user->member && $user->member->gender === 'male' ? 'selected' : '' }}>Male</option>
                            <option value="female" {{ $user->member && $user->member->gender === 'female' ? 'selected' : '' }}>Female</option>
                         </select>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success mt-3">Update Profile</button>
                    </div>
                    </form>

                    <script>
                        function previewImage(event) {
                            var input = event.target;
                            var preview = document.querySelector('.preview-image');
                    
                            if (input.files && input.files[0]) {
                                var reader = new FileReader();
                    
                                reader.onload = function (e) {
                                    preview.src = e.target.result;
                                };
                    
                                reader.readAsDataURL(input.files[0]);
                            }
                        }
                    </script>
                    
@endsection