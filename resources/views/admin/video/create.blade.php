<!-- tambah-gambar.blade.php -->
@extends('layout.admin.app')

@section('content')

<style>
    .thumbnail {
        max-width: 100%;
        max-height: 300px;
        margin-top: 10px;
    }
</style>

<div class="col-lg-12">
    <div class="card">
        <div class="card-header">
            <strong>Tambah Postingan</strong>
        </div>
        <div class="card-body card-block">
            <form method="POST" action="{{ route('store-v') }}" enctype="multipart/form-data" class="form-horizontal">
            @csrf
                <!-- Bagian Memilih Villa -->
                <div class="form-group">
                    <label for="villa_id" class="form-control-label">Pilih Penginapan</label>
                    <select name="villa_id" id="villa_id" class="form-control">
                        <option value="" disabled selected>Pilih Penginapan</option>
                        @foreach($villas as $villa)
                            <option value="{{ $villa->id }}">{{ $villa->Nama_Villa }}</option>
                        @endforeach
                    </select>
                    @error('villa_id')
                        <small class="text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="video" class="form-control-label">Pilih Media</label>
                    <div class="input-group">
                        <input type="file" name="media" id="mediaInput">
                    </div>
                </div>


                <!-- Tombol Submit -->
                <div class="form-actions form-group">
                    <button type="submit" name="click" class="btn btn-success btn-sm btn-block">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    document.addEventListener('DOMContentLoaded', function () {
        // Temukan elemen-elemen yang diperlukan
        var videoInput = document.getElementById('videoInput');
        var videoFileName = document.getElementById('videoFileName');
        var preview = document.getElementById('preview');

        // Tambahkan event listener untuk perubahan pada input file
        videoInput.addEventListener('change', function () {
            // Ambil nama file dari input file
            var fileName = videoInput.files[0].name;

            // Tampilkan nama file di elemen input text
            videoFileName.value = fileName;

            // (Opsional) Tampilkan preview video
            preview.innerHTML = '<p>Preview video: ' + fileName + '</p>';
        });
    });
</script>
@endsection
