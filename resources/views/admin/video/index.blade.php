@extends('layout.admin.app')

@section('content')
<style>
    .thumbnail {
        max-width: 200px; /* Atur lebar maksimum gambar */
        max-height: 150px; /* Atur tinggi maksimum gambar */
    }
    .center-td {
        text-align: center;
        vertical-align: middle !important;
    }
</style>

<div class="content">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-6">
                                <strong class="card-title">Postingan</strong>
                            </div>
                            <div class="col-md-6">
                                <div class="float-right">
                                    <a href="{{ route('videos-create') }}" class="btn btn-outline-success">Tambah</a>
                                </div>
                            </div>
                        </div>
                    </div><br>

                    <div class="table-responsive" style="margin: 15px 0; padding: 15px; background-color: #fff;">
                        <table class="table table-bordered ">
                            <thead>
                                <tr>
                                    <th class="center-td">Nomor</th>
                                    <th class="center-td">Penginapan</th>
                                    <th class="center-td">Media</th>
                                    <th class="center-td">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($vid as $v)
                                <tr>
                                    <td class="center-td">{{ $loop->iteration }}</td>
                                    <td class="center-td">
                                        @if($v->villa) {{-- Periksa apakah objek villa ada --}}
                                            {{ $v->villa->Nama_Villa }}
                                        @else
                                            Villa tidak tersedia
                                        @endif
                                    </td>
                                    <td class="center-td">
                                        <span class="thumbnail-text">{{ $v->media }}</span>
                                    </td>
                                    <td class="center-td">
                                    <a href="{{ route('destroyv', $v->id) }}" onclick="event.preventDefault(); if(confirm('Apakah Anda yakin ingin menghapus?')) {document.getElementById('delete-form-{{$v->id}}').submit();}" class="btn btn-outline-danger btn-sm">
                                            <i class="fa fa-trash-o"></i>
                                        </a>
                                        <form action="{{ route('destroyv', ['id' => $v->id]) }}" id="delete-form-{{$v->id}}" method="post">
                                            @csrf
                                            @method('DELETE')
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $vid->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div><!-- .animated -->
</div><!-- .content -->

<style>
    .thumbnail-text {
        display: inline-block;
        max-width: 200px; /* Atur lebar maksimum teks */
        overflow: hidden;
        text-overflow: ellipsis; /* Tambahkan elipsis jika teks melebihi lebar maksimum */
        white-space: nowrap;
    }
</style>

@endsection