@extends('layout.admin.app')

@section('content')

<div class="content">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-6">
                                <strong class="card-title">Tabel Layanan</strong>
                            </div>
                            <div class="col-md-6">
                                <div class="float-right">
                                    <a href=" {{ route('createL') }}" class="btn btn-outline-success">Tambah</a>
                                </div>
                                <form action="#" method="GET" class="form-inline">
                                <div class="form-group mx-sm-3">
                                    <input type="text" name="search"  value="" class="form-control" placeholder="Cari...">
                                </div>
                                <button type="submit" class="btn btn-primary">Cari</button>
                            </form>

                            </div>
                        </div>
                    </div><br>

                    <div class="table-responsive" style="margin: 15px 0; padding: 15px; background-color: ffffff;">
                        <table id="bootstrap-data-table" class="table table-bordered dataTable no-footer" role="grid" aria-describedby="bootstrap-data-table_info">
                            <thead >
                                <tr>
                                    <th style="text-align: center;">Nomor</th>
                                    <th style="text-align: center;">Layanan</th>
                                    <th style="text-align: center;">Deskripsi</th>
                                    <th style="text-align: center;">Status</th>
                                    <th style="text-align: center;">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            @php
                            $no = 1 + (( $layanan->currentPage() - 1) * $layanan->perPage());
                            @endphp
                                @foreach($layanan as $l)
                                <tr>
                                    <td style="text-align: center;">{{ $no++ }}</td>
                                    <td style="text-align: center;">{{ $l->layanan }}</tl>
                                    <td>{!! $l->deskripsi !!}</td>
                                    <td style="text-align: center;">
                                        <button class="btn {{ $l->status == 'aktif' ? 'btn-primary' : 'btn-success' }} text-white" style="border: none; padding: 5px 15px; border-radius: 8px;">
                                            {{ $l->status }}
                                        </button>
                                    </td>

                                    <td  style="text-align: center;"> 
                                         <a href="{{ route('showL', $l->id) }}"><i class="fa fa-eye"></i></a>
                                         <a href="{{ route('editL', $l->id) }}"><i class="fa fa-pencil"></i></a>
                                         <a href="{{ route('destroyL', $l->id) }}" onclick="event.preventDefault(); if(confirm('Apakah Anda yakin ingin menghapus?')) {document.getElementById('delete-form-{{$l->id}}').submit();}">
                                        <i class="fa fa-trash-o"></i>
                                    </a>
                                    <form action="{{ route('destroyL', ['id' => $l->id]) }}" id="delete-form-{{$l->id}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                    </form>
                                </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $layanan->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div><!-- .animated -->
</div><!-- .content -->





@endsection
