@extends('layout.admin.app')
  
@section('content')
<form method="POST" action="{{ route('storeL')}}" enctype="multipart/form-data" class="form-horizontal">
    @csrf
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header"><strong>Create Data</strong></div>
            <div class="card-body card-block">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="layanan" class="form-control-label">Layanan</label>
                            <input type="text" name="layanan" placeholder="Enter your company name" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="status" class="form-control-label">Status</label>
                            <select name="status" class="form-control">
                                <option value="aktif">Aktif</option>
                                <option value="nonaktif">Non Aktif</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="deskripsi" class="form-control-label">Deskripsi</label>
                            <textarea name="deskripsi" id="deskripsiTextarea" rows="9" placeholder="Content..." class="form-control"></textarea>
                        </div>
                    </div>
                </div>
                <div class="form-actions form-group">
                    <button type="submit" class="btn btn-success btn-sm btn-block" style="height: 40px;">Simpan</button>
                </div>
            </div>
        </div>
    </div>
</form>

@endsection
