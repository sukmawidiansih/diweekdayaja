@extends('layout.admin.app')

@section('content')
<form method="POST" action="{{ route('updateL', $layanan->id) }}" enctype="multipart/form-data" class="form-horizontal">
    @csrf
    @method('PUT') {{-- Gunakan metode PUT untuk update --}}
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header"><strong>Edit Data</strong></div>
            <div class="card-body card-block">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="layanan" class="form-control-label">Layanan</label>
                            <input type="text" name="layanan" value="{{ $layanan->layanan }}" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="status" class="form-control-label">Status</label>
                            <select name="status" class="form-control">
                                <option value="aktif" {{ $layanan->status == 'aktif' ? 'selected' : '' }}>Aktif</option>
                                <option value="nonaktif" {{ $layanan->status == 'nonaktif' ? 'selected' : '' }}>Non Aktif</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="deskripsi" class="form-control-label">Deskripsi</label>
                            <textarea name="deskripsi" id="deskripsiTextarea" rows="9" class="form-control">{{ $layanan->deskripsi }}</textarea>
                        </div>
                    </div>
                </div>
                <div class="form-actions form-group">
                    <button type="submit" class="btn btn-success btn-sm btn-block" style="height: 40px;">Update</button>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection
