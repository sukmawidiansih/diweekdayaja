@extends('layout.admin.app')

@section('content')

<div class="col-lg-12">
    <div class="card">
        <div class="card-header"><strong>Detail Villa</strong></div>
        <div class="card-body card-block">
            <div class="row">
                <div class="col-md-6">
                    <p><strong>Layanan:</strong> {{ $layanan->layanan }}</p>
                    <p><strong>Deskripsi:</strong> {!! $layanan->deskripsi !!}</p>
                    <p><strong>Status:</strong> {{ $layanan->status }}</p>
                </div>
            </div>

            <a href="{{ route('layanan') }}" class="btn btn-success mt-3">Kembali ke Daftar Layanan</a>
        </div>
    </div>
</div>
@endsection
