
@extends('layout.app')
@section('content')


</head>
   <style>
     .room {
    list-style: none;
  }
  .room a {
    cursor: pointer;
  }
  .ftco-social .ftco-animate a span,
    .ftco-social .ftco-animate a img {
        margin: 0 2px; 
        vertical-align: middle;
    }
    .ftco-social .ftco-animate a span {
        font-size: 18px; 
        color: #000;
    }
    .ftco-social .ftco-animate a img {
        width: 16px; 
        height: 16px; 
        filter: brightness(70%); 
    }
   
   </style>
   <body>
       
   <!-- Preloader / loading awal bgt -->
 <!-- <div id="preloader-active" >
    <div class="preloader d-flex align-items-center justify-content-center">
        <div class="preloader-inner position-relative">
            <div class="preloader-circle"></div>
            <div class="preloader-img pere-text">
            <img src="{{ asset('img/dwa2.png') }}" alt="Preloader Image">
         
                <strong></strong>
            </div>
        </div>
    </div>
</div> -->

<section class="home-slider owl-carousel">
    <div class="slider-item swiper-slide" style="background-image: url('{{ asset('img/home.jpg') }}');">
        <div class="overlay"></div>
        <div class="container">
            <div class="row no-gutters slider-text align-items-center justify-content-center">
                <div class="col-md-12 ftco-animate text-center">
                    <div class="text mb-5 pb-3">
                        <h2 class="ftco-heading-2">Selamat Datang !</h2>
                        <h1 class="mb-3">Di Weekday Aja</h1>
          <h2>Agen Reservasi Penginapan</h2>
        </div>
      </div>
    </div>
  </div>
</div>


      <div class="slider-item swiper-slide" style="background-image: url('{{ asset('img/home2.jpg') }}');">
      	<div class="overlay"></div>
        <div class="container">
          <div class="row no-gutters slider-text align-items-center justify-content-center">
          <div class="col-md-12 ftco-animate text-center">
          	<div class="text mb-5 pb-3">
	            <h1 class="mb-3">Nikmati Pengalaman Anda</h1>
	            <h2>Agen Reservasi Penginapan</h2>
            </div>
          </div>
        </div>
        </div>
      </div>
    </section>

    <div class="swiper-button-next"></div>
<div class="swiper-button-prev"></div>



    <section class="ftco-section ftc-no-pb ftc-no-pt">
			<div class="container">
				<div class="row">
					<div class="col-md-5 p-md-5 img img-2 d-flex justify-content-center align-items-center" style="background-image: url('{{ asset('img/abt.jpeg') }}');">
						<!-- <a  class="icon popup-vimeo d-flex justify-content-center align-items-center">
							<span class="icon-play"></span>
						</a> -->
					</div>
					<div class="col-md-7 py-5 wrap-about pb-md-5 ftco-animate">
	          <div class="heading-section heading-section-wo-line pt-md-5 pl-md-5 mb-5">
	          	<div class="ml-md-0">
		          	<span class="subheading">Tentang Kami</span>
		            <h2 class="mb-4">Di Weekday Aja</h2>
	           
	          <div class="pb-md-5">
							<p >Kami menyediakan penginapan beberapa Private Villa yang dapat Anda sewa secara harian.
                Kami menyediakan area untuk family gathering, garden party, 
                pool party. Kami juga menyediakan catering, paket BBQ, Penyewaan Sound Sistem dan penyewaan extra bed.</p>
							<p>Selain menyediakan privat Villa kami juga menyediakan penyewaan Transportasi untuk penjemputan dan pengantaran anda.</p>
							<ul class="ftco-social d-flex">
                                <li class="ftco-animate"><a href="https://www.facebook.com/rika.kurnia.940641"><span class="icon-facebook"></span></a></li>
                                <li class="ftco-animate"><a href="https://www.instagram.com/di_weekday_aja/"><span class="icon-instagram"></span></a></li>
                                <li class="ftco-animate"><a href="https://www.tiktok.com/@di_weekday_aja?is_from_webapp=1&sender_device=pc"><img src="{{ asset('image/tk.png') }}"></a></li>
                                <li class="ftco-animate"><a href="https://wa.me/+6281252255771"><img src="{{ asset('image/wa.png') }}"></a></li>
                            </ul>
						</div>
					</div>
				</div>
			</div>
		</section>

		
   
        <section class="ftco-section bg-light">
  <div class="container">
    <div class="row justify-content-center mb-5 pb-3">
      <div class="col-md-7 heading-section text-center ftco-animate">
        <h2 class="mb-4">Penginapan</h2>
      </div>
    </div>

    <div class="row">
      @foreach($dataVilla as $villa)
      <div class="col-sm col-md-6 col-lg-4 ftco-animate">
        <div class="room">
          <a href="">
          @php
                $gambar = $villa->gambarVilla->first();
            @endphp

            @if ($gambar)
                <a href="{{ route('roomsingle', $villa->id) }}" class="img d-flex justify-content-center align-items-center" style="background-image: url('{{ asset('image/' . $gambar->gambar) }}');">
                    <div class="icon d-flex justify-content-center align-items-center">
                        <span class="icon-search2"></span>
                    </div>
                </a>
            @endif
            <div class="text p-3 text-center">
              <h3 class="mb-3"><a href="" style="color: black; /* Set your desired text color */">{{ $villa->Nama_Villa }}</a></h3>
              <p><span class="price mr-2">Rp {{ $villa->Harga_Weekday }}</span> <span class="per">per malam</span></p>
              <hr>
              <p class="pt-1"><a href="{{ route('roomsingle', $villa->id) }}" class="btn-custom">Lihat Detail <span class="icon-long-arrow-right"></span></a></p>
            </div>
          </a>
        </div>
      </div>
      @endforeach
    </div>
    <div class="d-flex justify-content-center pt-3">
        <div>
            <a href="{{ route('rooms') }}" class="btn btn-success custom-button">Lihat Penginapan Lainnya</a>
        </div>
    </div>
  </div>
</section>


<section class="ftco-section">
    <div class="container">
        <div class="row justify-content-center mb-5 pb-3">
            <div class="col-md-7 heading-section text-center ftco-animate">
                <h2 class="mb-4">Layanan Kami</h2>
            </div>
        </div>
        <div class="row d-flex">

            <div class="col-md-4 d-flex align-self-stretch ftco-animate">
                <div class="media block-6 services py-4 d-block text-center">
                    <div class="d-flex justify-content-center">
                        <div class="icon d-flex align-items-center justify-content-center">
                            <span class="flaticon-spa"></span>
                        </div>
                    </div>
                    <div class="media-body p-2 mt-2">
                        <h3 class="heading mb-3">Penginapan</h3>
                        <p>Kami siap membantu Anda menemukan penginapan yang sesuai dengan kebutuhan Anda. 
                         Silakan berkonsultasi dengan tim kami untuk mendapatkan penginapan yang cocok dan nyaman.</p>
                    </div>
                </div>
            </div>

            <div class="col-md-4 d-flex align-self-stretch ftco-animate">
                <div class="media block-6 services py-4 d-block text-center">
                    <div class="d-flex justify-content-center">
                        <div class="icon d-flex align-items-center justify-content-center">
                            <span class="flaticon-reception-bell"></span>
                        </div>
                    </div>
                    <div class="media-body p-2 mt-2">
                        <h3 class="heading mb-3">Catering</h3>
                        <p>Kami menyediakan layanan catering untuk menyertai Anda selama menginap. Silakan hubungi tim kami untuk mendapatkan informasi lebih lanjut mengenai layanan catering kami. 
                           </p>
                    </div>
                </div>
            </div>

            <div class="col-md-4 d-flex align-self-stretch ftco-animate">
                <div class="media block-6 services py-4 d-block text-center">
                    <div class="d-flex justify-content-center">
                        <div class="icon d-flex align-items-center justify-content-center">
                            <span class="flaticon-car"></span>
                        </div>
                    </div>
                    <div class="media-body p-2 mt-2">
                        <h3 class="heading mb-3">Transportasi</h3>
                        <p>Kami menyediakan layanan transportasi untuk penjemputan dan pengantaran bagi Anda yang ingin menginap. Untuk informasi lebih lanjut, silakan hubungi tim kami. </p>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>


    <section class="ftco-section bg-light">
    <div class="container-fluid">
        <div class="row justify-content-center mb-5 pb-3">
            <div class="col-md-7 heading-section text-center ftco-animate">
                <h2>Blog</h2>
            </div>
        </div>
        <div class="row d-flex">
        @foreach($Blog as $b)
    <div class="col-md-3 d-flex ftco-animate">
        <div class="blog-entry align-self-stretch">
            <a href="{{ route('blogsingle', $b->id) }}" class="block-20" style="background-image: url('{{ asset('image/'. $b->gambar) }}'); width: 300px; height: 300px; background-size: cover; background-position: center;">
            </a>
            <div class="text mt-3 d-block">
                <h3 class="heading mt-3"><a href="{{ route('blogsingle', $b->id) }}">{{ $b->judul }}</a></h3>
                <div class="meta mb-3" >
                    <div><a href="#">{{ $b->date }}</a></div>
                    <div><a href="#">{{ $b->kategori->nama_kategori }}</a></div>
                </div>
            </div>
        </div>
    </div>
@endforeach
        </div>
        <div class="d-flex justify-content-center pt-3">
        <div>
            <a href="{{ route('blogs') }}" class="btn btn-success custom-button">Lihat Artikel Lainnya</a>
        </div>
    </div>
    </div>
</section>
<div class="untree_co-section">
    <div class="container">
        <div class="row mt-3 text-center justify-content-center mb-5">
            <div class=""><h2 class="section-title text-center">Galeri Penginapan</h2></div>
        </div>
        <div class="row">
            @foreach ($vid as $v)
                <div class="col-md-3 mb-1">
                    <div style="text-align: center;">
                        <video width="280" height="450" controls>
                            <source src="{{ asset($v->media) }}" type="video/mp4">
                        </video>
                    </div>
                    <div class="mt-2" style="text-align: center;">
                        <a href="{{ route('roomsingle', ['id' => $v->id]) }}">Lihat Penginapan</a>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>



<script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
<script>
    var swiper = new Swiper('.swiper-container', {
        slidesPerView: 1,
        spaceBetween: 10,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
    });
</script>

        <br><br><br><br><br>
    </main>
   
	@endsection