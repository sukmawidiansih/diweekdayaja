<?php
  
use Illuminate\Support\Facades\Route;
  
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\GambarController;
use App\Http\Controllers\KategoriController;
use App\Http\Controllers\BlogController;
use App\Http\Controllers\LayananController;
use App\Http\Controllers\ViewController;
use App\Http\Controllers\SettingController;
use App\Http\Controllers\TodoController; 
use App\Http\Controllers\VideoController;
  
  
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
  
Route::get('/',[ViewController::class, 'index'])->name('index');
Route::get('/rooms',[ViewController::class, 'rooms'])->name('rooms');
Route::get('/room/single/{id}',[ViewController::class, 'roomsingle'])->name('roomsingle');
Route::get('/search',[ViewController::class, 'search'])->name('search');
Route::get('/blogs',[ViewController::class, 'blogs'])->name('blogs');
Route::get('/blog/single/{id}',[ViewController::class, 'blogsingle'])->name('blogsingle');
Route::get('/contact',[ViewController::class, 'contact'])->name('contact');
Route::get('/about',[ViewController::class, 'about'])->name('about');
Route::get('/service',[ViewController::class, 'service'])->name('service');
  
Auth::routes();
  
/*------------------------------------------
--------------------------------------------
All Normal Users Routes List
--------------------------------------------
--------------------------------------------*/
Route::middleware(['auth', 'user-access:user'])->group(function () {
  
    Route::get('/home', [HomeController::class, 'index'])->name('home');
});
  
/*------------------------------------------
--------------------------------------------
All Admin Routes List
--------------------------------------------
--------------------------------------------*/
Route::middleware(['auth', 'user-access:admin'])->group(function () {
  
    Route::get('/admin/home', [HomeController::class, 'adminHome'])->name('admin.home');
    Route::group(['middleware' => 'admin-only'], function () {
        Route::get('/Data', [PostController::class, 'index'])->name('tabel_data');
        Route::get('/Create-Data', [PostController::class, 'create'])->name('create');
        Route::post('/Create-Store', [PostController::class, 'store'])->name('store');
        Route::get('/data-villa/detail/{id}', [PostController::class, 'show'])->name('detail');
        Route::get('/data-villa/edit/{id}', [PostController::class, 'edit'])->name('edit');
        Route::patch('/data-villa/update/{id}', [PostController::class, 'update'])->name('update');
        Route::delete('/data-villa/delete/{id}', [PostController::class, 'destroy'])->name('destroy');

        Route::get('/data-villa/bab/{id}', [GambarController::class, 'bab'])->name('bab');
        Route::get('/tambah/{id}', [GambarController::class, 'tambah'])->name('tambah');
        Route::post('/tambah/{id}/store', [GambarController::class, 'store'])->name('storeimg');;
        Route::delete('/gambar/delete/{id}', [GambarController::class, 'destroy'])->name('destroyImg');
        
        Route::get('/kategori', [KategoriController::class, 'kategori'])->name('kategori');
        Route::get('/kategori/create', [KategoriController::class, 'create'])->name('create-kat');
        Route::post('/kategori/store', [KategoriController::class, 'store'])->name('storekat');
        Route::delete('/kategori/{id}', [KategoriController::class, 'destroy'])->name('delete-kat');

        Route::get('/blog', [BlogController::class, 'blog'])->name('blog');
        Route::get('/Create-Data-blog', [BlogController::class, 'create'])->name('create-blog');
        Route::post('/Create-Store-blog', [BlogController::class, 'store'])->name('store-blog');
        Route::get('/blog/detail/{id}', [BlogController::class, 'detail'])->name('detail-blog');
        Route::get('/blog/edit/{id}', [BlogController::class, 'edit'])->name('edit-blog');
        Route::patch('/blog/update/{id}', [BlogController::class, 'update'])->name('update-blog');
        Route::delete('/blog/delete/{id}', [BlogController::class, 'destroy'])->name('destroy-blog');

        Route::get('/layanan', [LayananController::class, 'index'])->name('layanan');
        Route::get('/Create-Data-layanan', [LayananController::class, 'create'])->name('createL');
        Route::post('/Create-Store-layanan', [LayananController::class, 'store'])->name('storeL');
        Route::get('/layanan/edit/{id}', [LayananController::class, 'edit'])->name('editL');
        Route::put('/layanan/update/{id}', [LayananController::class, 'update'])->name('updateL');
        Route::get('/layanan/show/{id}', [LayananController::class, 'show'])->name('showL');
        Route::delete('/layanan/delete/{id}', [LayananController::class, 'destroy'])->name('destroyL');

        Route::get('/profile/admin', [SettingController::class, 'index'])->name('profile');
        Route::patch('/setting/admin', [SettingController::class, 'update'])->name('admin.setting.update');

        Route::get('/todos', [TodoController::class, 'index'])->name('todos.index');
        Route::get('/todos/create', [TodoController::class, 'create'])->name('create-todo');
        Route::post('/todo/store', [TodoController::class, 'store'])->name('todos.store');
        
        Route::get('/video', [VideoController::class, 'index'])->name('videos');
        Route::get('/video/create', [VideoController::class, 'create'])->name('videos-create');
        Route::post('/video/store', [VideoController::class, 'store'])->name('store-v');
        Route::delete('/video/destroy/{id}', [VideoController::class, 'destroy'])->name('destroyv');
    });
});
  
/*------------------------------------------
--------------------------------------------
All Admin Routes List
--------------------------------------------
--------------------------------------------*/
Route::middleware(['auth', 'user-access:manager'])->group(function () {
  
    Route::get('/manager/home', [HomeController::class, 'managerHome'])->name('manager.home');
});
 